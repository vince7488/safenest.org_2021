<?php
/**
* The template for displaying 404 pages (not found).
*
* @link https://codex.wordpress.org/Creating_an_Error_404_Page
*
* @package lawfirm
*/

get_header(); ?>

<main>

<section class="search-result-page not-found">

<div class="header-not-found">
  <div class="wrap">
    <h2>404: Page Not Found</h2>
    <p>Don't worry! Head over to our <a href="/">Home Page</a>, use the top navigation or enter what you are looking for in the search bar below.</p>
  </div>
</div>

<div class="search-bar">
  <div class="wrap">
    <form action="/" method="get">
      <input class="search-bar" placeholder="Search" value="" name="s" />
    </form>
    <p>Hit enter to search</p>
  </div>
</div>

</section>

</main>

<?php
get_footer();
