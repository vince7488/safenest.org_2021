<?php
/**
 * Block Name: Accordion Link List
 *
 * 
 */


 
// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-accordion-link-list.jpg" />


<?php 
// render the block in the browser
else : ?>

<section class="accordion-link-list">

    <div class="heading">
        <h2><?= get_field('accordion_link_list_headline'); ?></h2>
        <p><?= get_field('accordion_link_list_copy'); ?></p>
    </div>

<?php
if( have_rows('accordion_link_list_accordion_group') ):
    while( have_rows('accordion_link_list_accordion_group') ) : the_row(); ?>

        <span class="accordion wrap">
        <h6><?= get_sub_field('title');?></h6>
        <ul>
        <?php
        //find the list type
        $type = get_sub_field('link_list_or_text');

        //if it's links
        if($type == 'links'){
          // Loop over sub repeater rows.
          if( have_rows('links') ):
              while( have_rows('links') ) : the_row(); 
              
              $link = get_sub_field('link');
              $link_title = $link['title'];
              $link_title = $link['title'];
              $link_target = $link['target'] ? $link['target'] : '_self';
              ?>
                  <li><a href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>"><?= esc_html( $link_title ); ?></a></li>

              <?php endwhile;
          endif; 

        } elseif($type == 'text'){
          
          // Loop over sub repeater rows.
          if( have_rows('text') ):
            while( have_rows('text') ) : the_row(); ?>
            <li>
              <h3><?= the_sub_field('headline'); ?></h3>
              <p><?= the_sub_field('copy'); ?></p>
            </li>
          <?php endwhile;
          endif; 

        } else { 
          
          // Loop over sub repeater rows.
          if( have_rows('text') ):
            while( have_rows('text') ) : the_row(); ?>
          <li class="full">
            <h3><?= the_sub_field('headline'); ?></h3>
            <p><?= the_sub_field('copy'); ?></p>
          </li>

          <?php endwhile;
          endif; 
        } ?>
        </ul>
        </span>
    <?php endwhile;
endif; ?>

</section>



<?php endif; ?>