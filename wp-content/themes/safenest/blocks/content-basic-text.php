<?php
/**
 * Block Name: Basic Text
 *
 * This is the template that an p level element
 */



$copy = get_field('block_basic_text');
?>

<div class="basic-text wrap">

<?= $copy ?>

</div>