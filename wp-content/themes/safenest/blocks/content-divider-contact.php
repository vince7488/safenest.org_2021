<?php
/**
 * Block Name: Divider for Contact
 *
 * 
 */

// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-divider-contact.jpg" />


<?php 
// render the block in the browser
else : 



$title = get_field('divider_for_contact_title');
$call = get_field('divider_for_contact_call');
$schedule = get_field('divider_for_contact_schedule_a_pick_up_email');
$hours = get_field('divider_for_contact_donation_center_hours');
$email = get_field('divider_for_contact_email');
$address = get_field('divider_for_contact_address');
$map_link = get_field('divider_for_contact_google_maps_link');
?>

<section class="divider-contact wrap">

    <span>
        <h4><?= $title ?></h4>
    </span>
    <span>
        <ul>
            <li>
                <p><strong>Call:</strong>
                <a href="tel:<?= $call ?>"><?= $call ?></a></p>
            </li>
            <li>
                <p><strong>Schedule a Pick Up:</strong>
                <a href="mailto:<?= $schedule ?>"><?= $schedule ?></a></p>
            </li>
            <li>
                <p><strong>Donation Center Hours:</strong>
                <br/><?= $hours ?></p>
            </li>
        </ul>
    </span>
    <span>
        <ul>
            <li>
                <p><strong>Email:</strong>
                <a href="mailto:<?= $email ?>"><?= $email ?></a></p>
            </li>
            <li>
                <p><strong>Address:</strong>
                <a href="<?= $map_link ?>" target="_blank"><br/><?= $address ?></a></p>
            </li>
        </ul>
    </span>
    
</section>


<?php endif; ?>