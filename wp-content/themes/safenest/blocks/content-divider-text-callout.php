<?php
/**
 * Block Name: Divider w/ Text and a Button
 *
 * 
 */


 
// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-divider-text-callout.jpg" />


<?php 
// render the block in the browser
else : 

$color = get_field('divider_text_callout_background_color');

?>

<section class="divider-text-callout-<?= $color ?>">

    <div class="wrap">
        <span>
            <h2><?= get_field('divider_text_callout_headline') ?></h2>
            <p><?= get_field('divider_text_callout_copy') ?></p>
        </span>
        <span>
            <?php 
            $link = get_field('divider_text_callout_link');
            if( $link ){
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
                <a class="btn-green"  href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>"><?= esc_html( $link_title ); ?></a>
            <?php } ?>
        </span>
    </div>
    
</section>


<?php endif; ?>