<?php
/**
 * Block Name: Leadership List
 *
 * 
 */


// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-event-list.jpg" />


<?php 
// render the block in the browser
else : 


$headline = get_field('events_list_headline');
$copy = get_field('events_list_copy');

$block_type = get_field('events_list_number_of_events_to_show');
?>

<section class="events-list">

  <div class="wrap">
    <h2><?= $headline ?></h2>
    <p class="headline-copy"><?= $copy ?></p>
  </div>

  <?php if($block_type == "all"){
    safenest_load_all_event_posts();
  } else {
    safenest_load_three_event_posts();
  } ?>
    
</section>


<?php endif; ?>