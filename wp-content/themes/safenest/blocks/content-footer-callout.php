<?php
/**
 * Block Name: Footer Callout
 *
 * This is the block that includes a list of image links usually used above the footer
 */


 // render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-footer-callout.jpg" />


<?php 
// render the block in the browser
else :



$callout_type = get_field('footer_callout_type');
?>


<section class="footer-callout wrap">
    
<?php if($callout_type == 'five'){?>
    <div class="half-block">
        <?php if( have_rows('footer_callout_upper_block_copy') ): ?>
            <ul>
            <?php while( have_rows('footer_callout_upper_block_copy') ): the_row(); 
                $bg_image = get_sub_field('background_image');
                $link = get_sub_field('link');
                if( $link ){ 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                }
                ?>
                <li style="background: center / cover no-repeat #9BC76E url('<?= esc_url($bg_image['url']); ?>'); background-blend-mode: luminosity;">
                    <a href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>">
                        <span class="cover"></span>
                        <h3><?= esc_html( $link_title ); ?></h3>
                    </a>
                </li>
            <?php endwhile; ?>
            </ul>
        <?php endif; ?>
    </div>
    <?php } ?>

    <div class="half-block">
        <?php if( have_rows('footer_callout_bottom_block') ): ?>
            <ul>
            <?php while( have_rows('footer_callout_bottom_block') ): the_row(); 
                $bg_image = get_sub_field('background_image');
                $link = get_sub_field('link');
                if( $link ){ 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                }
                ?>
                <li style="background: center / cover no-repeat #9BC76E url('<?= esc_url($bg_image['url']); ?>'); background-blend-mode: luminosity;">
                    <a href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>">
                        <span class="cover"></span>
                        <h3><?= esc_html( $link_title ); ?></h3>
                    </a>
                </li>
            <?php endwhile; ?>
            </ul>
        <?php endif; ?>
    </div>

</section>



<?php endif; ?>