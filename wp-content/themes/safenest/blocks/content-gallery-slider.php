<?php
/**
 * Block Name: Gallery Slider
 *
 * This is the template for creating full width gallery/slider
 */

 
// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-gallery-slider.jpg" />


<?php 
// render the block in the browser
else : ?>

<div class="gallery-slider">
<div id="gallery">
        <?php if( have_rows('slide_type') ):
            while( have_rows('slide_type') ): the_row(); ?>

              <?php 
              if( get_row_layout() == 'image' ): 
                  $image = get_sub_field('image');
                  $headline = get_sub_field('headline');
                  $copy = get_sub_field('copy');
                  $cta = get_sub_field('cta');
                  $cta_url = $cta['url'];
                  $cta_title = $cta['title'];
                  $cta_target = $cta['target'] ? $cta['target'] : '_self';
              ?>
                  <div>
                      <div class="image">
                          <span class="text">
                              <?php 
                              if($headline){ ?>
                                <h2><?= $headline ?></h2>
                              <?php } 
                              if($copy){
                              ?>
                                <p><?= $copy ?></p>
                              <?php } 
                              if($cta){ ?>
                                <a class="btn-purple-sm cta" href="<?= esc_url($cta_url); ?>" target="<?= esc_attr($cta_target); ?>"><?= $cta_title; ?></a>
                              <?php } ?>
                          </span>
                          <span class="cover" style="background: black url(<?= esc_url($image['url']); ?>) center no-repeat; background-size:cover;">
                          </span>
                      </div>
                  </div>


              <?php 
              elseif( get_row_layout() == 'full_image_link' ): 
                  $image = get_sub_field('image');
                  
                  $link = get_sub_field('link');
                  $link_url = $link['url'];
                  $link_target = $link['target'] ? $link['target'] : '_self';
              ?>
                  <div>
                    <a href="<?= esc_url($link_url); ?>" target="<?= esc_attr($link_target); ?>">
                      <div class="image" style="background: black url(<?= esc_url($image['url']); ?>) center no-repeat; background-size:cover;"></div>
                    </a>
                  </div>


              <?php 
              elseif( get_row_layout() == 'video' ): 
                  $video = get_sub_field('video_file');
                  $poster = get_sub_field('poster_image');
                  $headline = get_sub_field('headline');
                  $copy = get_sub_field('copy');
                  ?>
                  <div>
                      <div class="video">
                      <span class="text">
                              <?php 
                              if($headline){ ?>
                                <h2><?= $headline ?></h2>
                              <?php } 
                              if($copy){
                              ?>
                                <p><?= $copy ?></p>
                              <?php } ?>
                          </span>
                          <img src="<?= get_template_directory_uri() ?>/src/icons/play_button.svg" />
                          <span class="cover"></span>
                          <video id="video-1" poster="<?= $poster['url']; ?>" controls>
                              <source src="<?= $video['url']; ?>" type="video/mp4"></source>
                          </video> 
                      </div>
                  </div>


              <?php 
              elseif( get_row_layout() == 'you_tube' ): 
                  $embed = get_sub_field('embed_code');
                  ?>
                  <div>
                      <?= $embed ?>
                  </div>
              <?php 
              endif; 
              
            endwhile;
        endif; ?>
    </div>
</div>



<?php endif; ?>