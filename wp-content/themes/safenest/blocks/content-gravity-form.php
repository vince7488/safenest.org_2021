<?php
/**
 * Block Name: Hubspot
 *
 * This is a list of text items that are displayed in two columns
 */

// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-gravity-form.jpg" />


<?php 
// render the block in the browser
else : 



$title = get_field('block_gravity_title');
$copy = get_field('block_gravity_copy');

//gravity data
$form_id = get_field('block_gravity_form_id');
?>



<section class="gravity-form wrap">

    <?php if($title){ ?>
      <h2><?= $title ?></h2>
    <?php }
    if($copy) { ?>
      <p><?= $copy ?></p>
    <?php } ?>

    <?php gravity_form( $form_id, false, false , false, null, true, '1', $echo = true ); ?>
    
</section>



<?php endif; ?>