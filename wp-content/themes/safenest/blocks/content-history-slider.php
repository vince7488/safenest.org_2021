<?php
/**
 * Block Name: History Slider
 *
 * 
 */
// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-history-slider.jpg" />


<?php 
// render the block in the browser
else : 


$history_copy = get_field('history_block_copy');
?>

<section class="history-slider">
    <div class="heading">
        <h2>History</h2>
        <p><?= $history_copy ?></p>
    </div>

    <div class="wrap">
        <div class="history-slider" id="history">
            <?php 
                $args = array(
                'order' => 'DESC',
                'orderby' => 'menu_order',
                'posts_per_page' => -1,
                'post_type' => 'History',
                );
                $history_posts = new WP_Query($args);

                if ( $history_posts->have_posts() ): 
                while ( $history_posts->have_posts() ): $history_posts->the_post();
                
                $post_id = get_the_ID();
                $year = get_field('history_year', $post_id);
                $snippet = get_field('history_snippet', $post_id);
                ?>
                <div>
                    <h5><?= $year ?></h5>
                    <p><?= $snippet ?></p>
                </div>
                <?php 
                endwhile; 
                wp_reset_postdata();
            endif;
            ?>
        </div>
    </div>
    <div class="slick-arrow prev"></div>
    <div class="slick-arrow next"></div>

    <div class="swipe-icon">
        <img src="<?= get_template_directory_uri() ?>/src/icons/swipe.svg" alt="swipe">
    </div>

</section>


<?php endif; ?>