<?php
/**
 * Block Name: Home Hero
 *
 * This is the template for creating the home page hero
 */


// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-home-hero.jpg" />


<?php 
// render the block in the browser
else : 


$headline = get_field('home_hero_headline');
$copy = get_field('home_hero_copy');

$include_button = get_field('home_hero_include_button');
if($include_button){
    $link = get_field('home_hero_link');
    $link_url = $link['url'];
    $link_title = $link['title'];
    $link_target = $link['target'] ? $link['target'] : '_self';
}

//background images
$desktop_background = get_field('home_hero_background_image_desktop');
$mobile_background = get_field('home_hero_background_image_mobile');
?>

<style>
 .bg-image{
  background: #a979d4 url(<?= $desktop_background['url'] ?>) left no-repeat;
  background-blend-mode: multiply;
  background-size:cover;
 }
 @media screen and (max-width: 600px) {
  .bg-image{
    background: #9f59dc url(<?= $mobile_background['url'] ?>) bottom no-repeat;
  }
} 
</style>
	
<section class="home-hero">
    <div class="bg-image">
        <span></span>
        <div class="text">
            <h1><?= $headline ?></h1>
            <p><?= $copy ?></p>
            <?php
            if($include_button){ ?>
                <a class="btn-green" href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>"><?= esc_html( $link_title ); ?></a>
            <?php } ?>
        </div>
    </div>
    
    <div class="links">
    <?php if( have_rows('home_hero_links_blocks') ): ?>
        <ul class="wrap">
        <?php while( have_rows('home_hero_links_blocks') ) : the_row();

            $title = get_sub_field('title'); 
            $copy = get_sub_field('copy'); 

            $button = get_sub_field('button'); 
            $button_url = $button['url'];
            $button_title = $button['title'];
            $button_target = $button['target'] ? $button['target'] : '_self';
            ?>

            <li>
                <h3><?= $title ?></h3>
                <p><?= $copy ?></p>
                <a class="btn-purple-sm" href="<?= esc_url( $button_url ); ?>" target="<?= esc_attr( $button_target ); ?>"><?= esc_html( $button_title ); ?></a>
            </li>

        <?php endwhile; ?>
        </ul>
    <?php endif; ?>
    </div>
</section>

<?php endif; ?>