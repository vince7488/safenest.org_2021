<?php
/**
 * Block Name: Hubspot
 *
 * This is a list of text items that are displayed in two columns
 */

// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-gravity-form.jpg" />


<?php 
// render the block in the browser
else : 



$title = get_field('block_hubspot_title');
$copy = get_field('block_hubspot_copy');

//hubspot data
$region = get_field('block_hubspot_region');
$portal_id = get_field('block_hubspot_portal_id');
$form_id = get_field('block_hubspot_form_id');
?>



<section class="hubspot wrap">

    <?php if($title){ ?>
      <h2><?= $title ?></h2>
    <?php }
    if($copy) { ?>
      <p><?= $copy ?></p>
    <?php } ?>
    <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/shell.js"></script>
    <script>
      hbspt.forms.create({
      region: "<?= $region ?>",
      portalId: "<?= $portal_id ?>",
      formId: "<?= $form_id ?>"
    });
    </script>
    
</section>



<?php endif; ?>