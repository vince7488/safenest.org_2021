<?php
/**
 * Block Name: Image Stack
 *
 * This is the template for creating an image stack with a text description
 */
 // render the example image pop-up in the gutenburg admin
 if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-image-stack.jpg" />


<?php 
// render the block in the browser
else : ?>




<div class="image-stack">

<?php

$stack_type = get_field('layout_image_stack_type');

if( have_rows('layout_image_stack') ): ?>

    <ul class="wrap stack-<?= $stack_type ?>">

    <?php 
    while( have_rows('layout_image_stack') ) : the_row();

        
        $headline = get_sub_field('headline'); 
        $copy = get_sub_field('copy'); 
        $link = get_sub_field('link'); 
        $image = get_sub_field('image'); 
        ?>

        <li>
            <img src="<?= esc_url($image['url']); ?>" alt="<?= esc_attr($image['alt']); ?>" />
            <span>
                <?php if($headline != ""){ ?>
                <h2><?= $headline ?></h2>
                <?php } ?>
                <p><?= $copy ?></p>

                <?php if( $link ){ 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
                    <a class="link" href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>"><?= esc_html( $link_title ); ?></a>
                <?php } ?>
            </span>
        </li>
       
    <?php endwhile; ?>
    </ul>
<?php endif; ?>
</div>



<?php endif; ?>