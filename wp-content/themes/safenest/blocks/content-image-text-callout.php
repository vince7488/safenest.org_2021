<?php
/**
 * Block Name: Image with Text Callout
 *
 * This is the block that includes an image with a text overlay and text on the side
 */


 // render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-image-text-callout.jpg" />


<?php 
// render the block in the browser
else :

 $background = get_field('image_text_callout_image_side_image');
 $link = get_field('image_text_callout_text_side_link');
?>

<div class="image-text-callout wrap">

    <div class="image-side" style="background: url('<?= esc_url($background['url']); ?>') left no-repeat; background-size:cover; background-color: #9BC76E; background-blend-mode: multiply;">
        <span class="cover"></span>
        <div class="text">
            <h3><?= the_field('image_text_callout_image_side_image_headline') ?></h3>
            <?php if( have_rows('image_text_callout_image_side_image_text') ): ?>
                <?php while( have_rows('image_text_callout_image_side_image_text') ): the_row(); 
                    $title = get_sub_field('title');
                    $figure = get_sub_field('figure');
                    ?>
                    <span>
                        <p class="title"><?= $title ?></p>
                        <p class="figure"><?= $figure ?></p>
                    </span>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>


    <div class="text-side">
        <h2><?= the_field('image_text_callout_text_side_headline') ?></h2>
        <p><?= the_field('image_text_callout_text_side_copy') ?></p>
        <?php 
        if( $link ){ 
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
        ?>
        <a class="link" href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>"><?= esc_html( $link_title ); ?></a>
        <?php } ?>  
    </div>

</div>



<?php endif; ?>