<?php
/**
 * Block Name: Image with Text
 *
 * This is the block that includes an image that with text on the side
 */



 // render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-image-with-text.jpg" />


<?php 
// render the block in the browser
else :


$image = get_field('image_text_image_side_image');
$link = get_field('image_text_text_side_link');

    
$url = $image['url'];
$alt = $image['alt'];
$size = 'lawfirm_img_square';
$thumb = $image['sizes'][ $size ];
?>

<div class="image-with-text wrap">

    <div class="image-side" style="background: url('<?= esc_url($image['url']); ?>') center no-repeat; background-size:cover;">
        <span class="cover"></span>
        <img src="<?= esc_url($thumb); ?>" alt="<?= esc_attr($alt); ?>" />
    </div>


    <div class="text-side">
        <?php 
        $headline = get_field('image_text_text_side_headline');
        $copy = get_field('image_text_text_side_copy');
        
        if($headline){ ?>
        <h2><?= $headline ?></h2>
        <?php } ?>
        <p><?= $copy ?></p>
        <?php 
        if( $link ){ 
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
        ?>
        <a class="link" href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>"><?= esc_html( $link_title ); ?></a>
        <?php } ?>  
    </div>

</div>


<?php endif; ?>