<?php
/**
 * Block Name: Internal Hero
 *
 * the hero that should be used on all internal pages
 */


 // render the example image pop-up in the gutenburg admin
 if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-internal-hero.jpg" />


<?php 
// render the block in the browser
else :

$headline = get_field('internal_page_hero_headline');
$copy = get_field('internal_page_hero_copy');

$include_button = get_field('internal_page_hero_include_button');
if($include_button){
    $link = get_field('internal_page_hero_link');
    $link_url = $link['url'];
    $link_title = $link['title'];
    $link_target = $link['target'] ? $link['target'] : '_self';
}

$include_callout = get_field('internal_page_hero_include_callout');

//background images
$desktop_background = get_field('internal_page_hero_background_image');
$mobile_background = get_field('internal_page_hero_background_image_mobile');
?>

<style>
 .bg-image{
  background: #57217966 url(<?= $desktop_background['url'] ?>) center no-repeat;
  background-blend-mode: multiply;
  background-size:cover;
 }
 @media screen and (max-width: 600px) {
  .bg-image{
    background: #a973d8 url(<?= $mobile_background['url'] ?>) top no-repeat;
    background-blend-mode: multiply;
    background-size:cover;
  }
} 
</style>

<section class="hero-internal">
  <div class="bg-image <?php if(!$include_callout){ echo "no-callout";}?>">
        <span></span>
        <div class="text">
            <h1><?= $headline ?></h1>
            <p><?= $copy ?></p>

            <?php
            if($include_button){ ?>
                <a class="btn-green" href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>"><?= esc_html( $link_title ); ?></a>
            <?php } ?>

        </div>
    </div>
    <?php
    if($include_callout){ 
      
    $headline = get_field('internal_page_hero_callout_title');
    $subheadline = get_field('internal_page_hero_callout_title_copy');
    $tel = get_field('internal_page_hero_callout_phone_number');

    $callout_btn = get_field('internal_page_hero_callout_link');
    if($callout_btn){
      $callout_btn_url = $callout_btn['url'];
      $callout_btn_title = $callout_btn['title'];
      $callout_btn_target = $callout_btn['target'] ? $callout_btn['target'] : '_self';
    }
    ?>
    <div class="hero-callout wrap">
        <span>
            <?php if($headline) { ?>
              <h2><?= $headline ?></h2>
            <?php } ?>
            <?php if($subheadline) { ?>
              <p><?= $subheadline ?></p>
            <?php } ?>
        </span>
        <span>
            <?php if($callout_btn) { ?>
              <a class="btn-purple" href="<?= esc_url( $callout_btn_url ); ?>" target="<?= esc_attr( $callout_btn_target ); ?>"><?= esc_html( $callout_btn_title ); ?></a>
            <?php } ?>
            <?php if($tel) { ?>
              <a href="tel:<?= $tel ?>" class="tel"><?= $tel ?></a>
            <?php } ?>
        </span>
        <p></p>
    </div>
    <?php } ?>
</section>


<?php endif; ?>