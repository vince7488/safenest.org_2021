<?php
/**
 * Block Name: Leadership List
 *
 * 
 */


// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-leadership-list.jpg" />


<?php 
// render the block in the browser
else : 


$headline = get_field('leadership_list_header');
?>

<section class="leadership-list">

    <div class="wrap">
    <h2><?= $headline ?></h2>

    <ul>
    <?php
    if( have_rows('leadership_list_people_list') ):
        while ( have_rows('leadership_list_people_list') ) : the_row(); 
        
        $name = get_sub_field('name');
        $position = get_sub_field('position');
        $company = get_sub_field('company');

        //profile pic
        $profile_pic = get_template_directory_uri() . '/src/img/blank-profile.png';
        $image = get_sub_field('image');
        if($image){
          $profile_pic = $image['url'];
        }
        ?>

        <li>
          <div class="profile" style="background:url('<?= $profile_pic ?>') center no-repeat; background-size:cover;">
            <img src="<?= $profile_pic ?>" alt="<?= $name ?>">
          </div>
          <h4><?= $name ?></h4>
          <p><?= $position ?></p>
          <p><?= $company ?></p>
        </li>

        <?php
        endwhile;
    endif; ?>
    </ul>
    
</section>


<?php endif; ?>