<?php
/**
 * Block Name: Leadership List
 *
 * 
 */


// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-location-list.jpg" />


<?php 
// render the block in the browser
else : 


$headline = get_field('location_list_headline');
$sub_copy = get_field('location_list_sub_copy');
?>

<section class="location-list">

  <div class="wrap">
    <h2><?= $headline ?></h2>
    <p><?= $sub_copy ?></p>

    <?php
    if( have_rows('location_list') ): ?>
    <div class="locations-grid">
      <?php while ( have_rows('location_list') ) : the_row(); 
      
      
      $name = get_sub_field('name');
      $city = get_sub_field('city');
      $phone = get_sub_field('phone');
      $address = get_sub_field('address');
      $state = get_sub_field('state');
      $zip = get_sub_field('zip_code');
      $map = get_sub_field('google_map_link');
      ?>

      <div class="block">
        <h4><?= $name ?></h4>
        <p class="phone"><?= $phone ?></p>
        <p class="address"><?= $address ?><br /><?= $city ?>, <?= $state ?> <?= $zip ?></p>
        <a href="<?= $map ?>" class="link" target="_blank">Get Directions</a>
      </div>

      <?php endwhile; ?>
    </div>
  <?php endif; ?>
  </div>
    
</section>


<?php endif; ?>