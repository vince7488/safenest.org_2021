<?php
/**
 * Block Name: News Stories
 *
 * 
 */
// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-news-stories.jpg" />


<?php 
// render the block in the browser
else : 




?>
  <section class="news-stories wrap">
    <span class="post-container" data-total="<?= safenest_get_total_news_page_count() ?>">
      <?php safenest_load_news_posts(); ?>
    </span>
    <div class="btn-purple load-more" data-page="1">Load More</div>
  </section>
<?php endif; ?>