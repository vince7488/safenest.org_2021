<?php
/**
 * Block Name: Number Callout
 *
 * T
 */
// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-number-callout.jpg" />


<?php 
// render the block in the browser
else : ?>



<section class="number-callout">
<?php if( have_rows('block_number_callout') ) { ?>
    <div class="wrap">
    <?php while( have_rows('block_number_callout') ): the_row(); ?>
        <span>
            <p class="figure"><?= get_sub_field('number'); ?></p>
            <p class="caption"><?= get_sub_field('caption'); ?></p>
        </span>
    <?php endwhile; ?>
    </div>
<?php } ?>
</section>


<?php endif; ?>