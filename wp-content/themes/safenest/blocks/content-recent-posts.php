<?php
/**
 * Block Name: Recent Posts
 *
 * This is the block that will display the 3 most recent posts
 */


 
// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-recent-posts.jpg" />


<?php 
// render the block in the browser
else : ?>

<div class="recent-posts wrap">

    <?php 
    
    $news_args = array(
        'order' => 'DESC',
        'posts_per_page' => 3,
        'paged' => 1
    );
    $news_posts = new WP_Query($news_args); 
    if ( $news_posts->have_posts() ): 
        while ( $news_posts->have_posts() ): $news_posts->the_post(); 


        $post_id = get_the_ID();
        $snippet = get_field('news_post_snippet', $post_id);
        $title = get_the_title();
        $date = get_the_date();

        $card_image = get_field('news_post_card_image', $post_id);
        $url = $card_image['url'];
        $alt = $card_image['alt'];
        $size = 'lawfirm_img_square';
        $thumb = $card_image['sizes'][ $size ];

        //get link and set target
        $type = get_field('news_post_post_type', $post_id);
        $link = get_permalink();
        $target = "_self";
        if($type == "external_video" || $type == "external_story"){
          $link = get_field('news_post_external_url', $post_id);
          $target = "_blank";
        }
    ?>

        <article>
            <a href="<?= $link ?>" target="<?= $target ?>">
                <img src="<?= esc_url($thumb); ?>" alt="<?= esc_attr($alt); ?>" />
            </a>
            <p class="date"><?= $date ?></p>
            <a href="<?= $link ?>" target="<?= $target ?>">
                <h3><?= $title ?></h3>
            </a>
            <p class='snippet'><?= $snippet ?></p>
        </article>

    
        <?php endwhile; 
        wp_reset_postdata();
  endif; ?>
    

</div>


<?php endif; ?>