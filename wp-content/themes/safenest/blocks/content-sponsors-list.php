<?php
/**
 * Block Name: Sponsors List
 *
 * 
 */
// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-sponsors-list.jpg" />


<?php 
// render the block in the browser
else : ?>
<section class="sponsors-list wrap">

    <span>
        <h2><?= get_field('sponsors_list_title') ;?></h2>
        <?= get_field('sponsors_list_copy') ;?>
        <?php 
        $link = get_field('sponsors_list_link');
        if( $link ){
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
            ?>
            <a class="link"  href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>"><?= esc_html( $link_title ); ?></a>
        <?php } ?>
    </span>

    <span>

        <?php if( have_rows('sponsors_list_logo_list') ): ?>
            <ul>
            <?php while( have_rows('sponsors_list_logo_list') ): the_row(); ?>
                <li>
                    <?php $image = get_sub_field('image'); ?>
                    <img src="<?= esc_url($image['url']); ?>" alt="<?= esc_attr($image['alt']); ?>" />
                </li>
            <?php endwhile; ?>
            </ul>
        <?php endif; ?>
    </span>

</section>
<?php endif; ?>