<?php
/**
 * Block Name: Tabbed Image with Text
 *
 * This is the block that ius tabbed with an image and text on the side
 */


 
// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-tabbed-image-with-text.jpg" />


<?php 
// render the block in the browser
else : ?>



<section class="tabbed-image-with-text wrap">

    <?php if( have_rows('tabbed_image_w_text_tabs') ): 
        $tab_counter = 0;
        $tab_active = 'active';
    ?>
    <div class="tabs">
        <ul>
        <?php while( have_rows('tabbed_image_w_text_tabs') ) : the_row(); 
        $title = get_sub_field('text_side_headline');
        ?>
            <li id="tab-<?= $tab_counter ?>" class="<?= $tab_active ?>"><?= $title ?></li>
        <?php 
        $tab_counter ++;
        $tab_active = '';
        endwhile; 
        ?>
        </ul>
    </div>
    <?php endif; ?>


    <?php if( have_rows('tabbed_image_w_text_tabs') ): 
        
        $counter = 0;
        $active = 'active-tab';
         
        while( have_rows('tabbed_image_w_text_tabs') ) : the_row();

            $image = get_sub_field('image_side_image'); 
            $headline = get_sub_field('text_side_headline'); 
            $copy = get_sub_field('text_side_copy'); 
            $link = get_sub_field('text_side_link'); 
            ?>
            <span class="tab-<?= $counter ?> <?= $active ?>">
                
                <div class="text-side">
                    <h2><?= $headline ?></h2>
                    <p><?= $copy ?></p>
                    <?php 
                    if( $link ){ 
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
                    <a class="link" href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>"><?= esc_html( $link_title ); ?></a>
                    <?php } ?>  
                </div>
                <div class="image-side">
                    <img src="<?= $image['url'] ?><?= $image['alt'] ?>">
                </div>
            </span>
        <?php 
        $counter ++;
        $active = '';
        endwhile; 
    endif; ?>


</section>




<?php endif; ?>