<?php
/**
 * Block Name: Testimonial Slider
 *
 * A slider that displays testimonials
 * Testimonials come from a custom post type 
 * this was changed from testimonials to Survivor Messages of Hope
 * 
 */
 // render the example image pop-up in the gutenburg admin
 if (get_field('is_example')) : ?>


  <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-testimonial-slider.jpg" />


<?php 
// render the block in the browser
else : ?>



<section class="testimonial-slider">

  <h4>Survivor Messages of Hope</h4>

  <div id="testimonials">

    <?php 
    $args = array(
      'order' => 'DESC',
      'posts_per_page' => -1,
      'post_type' => 'testimonials',
    );
    $testimonial_posts = new WP_Query($args);

    if ( $testimonial_posts->have_posts() ): 
      while ( $testimonial_posts->have_posts() ): $testimonial_posts->the_post();
      
      $post_id = get_the_ID();
      $name = get_field('testimonial_full_name', $post_id);
      $copy = get_field('testimonial_copy', $post_id);
      ?>
      <div>
        <p><?= $copy ?></p>
        <?php if($name){ ?>
          <p class="name">- <?= $name ?></p>
        <?php } else {?>
          <p class="name"></p>
        <?php } ?>
      </div>
    <?php 
      endwhile; 
      wp_reset_postdata();
   endif;
  ?>


  </div>
</section>

<?php endif; ?>