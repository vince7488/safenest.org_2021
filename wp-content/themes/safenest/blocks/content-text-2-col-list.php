<?php
/**
 * Block Name: Text 2 Col List
 *
 * This is a list of text items that are displayed in two columns
 */

// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-text-2-col-list.jpg" />


<?php 
// render the block in the browser
else : ?>



<section class="text-2-col-list wrap">

    <h2><?= get_field('two_col_text_list_title') ?></h2>
    <?php if( have_rows('two_col_text_list_list') ): ?>
        <ul>
        <?php while( have_rows('two_col_text_list_list') ): the_row(); ?>
            <li>
                <h3><?= the_sub_field('headline'); ?></h3>
                <p><?= the_sub_field('copy'); ?></p>
            </li>
        <?php endwhile; ?>
        </ul>
    <?php endif; ?>
    
</section>



<?php endif; ?>