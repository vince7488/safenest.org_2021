<?php
/**
 * Block Name: Text Builder
 *
 * 
 */


// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-text-builder.jpg" />


<?php 
// render the block in the browser
else : ?>

<section class="text-builder">

    <div class="wrap">
    <?php
    if( have_rows('safenest_text_builder') ):
        while ( have_rows('safenest_text_builder') ) : the_row();

            // Case: headline
            if( get_row_layout() == 'headline' ):
                $headline = get_sub_field('headline'); ?>
                <h2><?= $headline ?></h2>

            <?php
            // Case: subheadline
            elseif( get_row_layout() == 'subheadline' ): 
                $subheadline = get_sub_field('sub_headline'); ?>
                <h5><?= $subheadline ?></h5>

            <?php
            // Case: text block
            elseif( get_row_layout() == 'text_block' ): 
                $text_block = get_sub_field('text_block'); ?>
                <?= $text_block ?>

            <?php
            // Case: list
            elseif( get_row_layout() == 'list' ): 
                $list = get_sub_field('list'); ?>
               <ul>
                <?php
                if( have_rows('list') ):
                  while ( have_rows('list') ) : the_row(); ?>

                    <li><?= the_sub_field('item'); ?></li>

                  <?php 
                  endwhile;
                endif;
                ?>
               </ul>

            <?php
            // Case: button
            elseif( get_row_layout() == 'button' ): 
                $list = get_sub_field('button'); 

                $link = get_sub_field('button');
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
                <a class="btn-green"  href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>"><?= esc_html( $link_title ); ?></a>

            <?php
            endif;
        endwhile;
    endif; ?>
    </div>
    
</section>


<?php endif; ?>