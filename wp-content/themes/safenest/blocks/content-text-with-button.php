<?php
/**
 * Block Name: Text with Button
 *
 * a single or double text area with a button link
 */
// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-text-with-button.jpg" />


<?php 
// render the block in the browser
else :


    
$cols = get_field('text_with_button_columns');

//two columns of text and button action
if ($cols == "two"){
?>
<section class="text-with-button wrap">
    <span>
        <h3><?= get_field('text_with_button_left_side_headline') ?></h3>
        <p><?= get_field('text_with_button_left_side_copy') ?></p>

        <?php 
        $link = get_field('text_with_button_left_side_button');
        if( $link ){
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
        }
        ?>
        <a class="btn-green" href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>"><?= esc_html( $link_title ); ?></a>
    </span>

    <span>
    <h3><?= get_field('text_with_button_right_side_headline') ?></h3>
        <p><?= get_field('text_with_button_right_side_copy') ?></p>

        <?php 
        $link_r = get_field('text_with_button_right_side_button');
        if( $link_r ){
            $link_r_url = $link_r['url'];
            $link_r_title = $link_r['title'];
            $link_r_target = $link_r['target'] ? $link_r['target'] : '_self';
        }
        ?>
        <a class="btn-green" href="<?= esc_url( $link_r_url ); ?>" target="<?= esc_attr( $link_r_target ); ?>"><?= esc_html( $link_r_title ); ?></a>
    </span>
</section>

<?php }  
//one column of text and button solo
else {?>

<section class="text-with-button wrap">
    <span class="full">
        <h3><?= get_field('text_with_button_left_side_headline') ?></h3>
        <p><?= get_field('text_with_button_left_side_copy') ?></p>

        <?php 
        $link = get_field('text_with_button_left_side_button');
        if( $link ){
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
        }
        ?>
        <a class="btn-green" href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>"><?= esc_html( $link_title ); ?></a>
    </span>
</section>
<?php } ?>



<?php endif; ?>