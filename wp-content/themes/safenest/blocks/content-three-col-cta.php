<?php
/**
 * Block Name: Three Column CTA
 *
 * 
 */


 
// render the example image pop-up in the gutenburg admin
if (get_field('is_example')) : ?>


    <img src="<?= get_template_directory_uri() ?>/blocks/examples/content-recent-posts.jpg" />


<?php 
// render the block in the browser
else : ?>

<div class="three-col-cta wrap">

  <?php
  if( have_rows('3_col_cta_blocks') ):
      while( have_rows('3_col_cta_blocks') ) : the_row(); 
      
      $headline = get_sub_field('headline');
      $copy = get_sub_field('copy');

      //button
      $link = get_sub_field('button');
      $link_url = $link['url'];
      $link_title = $link['title'];
      $target = $link['target'] ? $link['target'] : '_self';

      //image
      $image = get_sub_field('image');
      $url = $image['url'];
      $alt = $image['alt'];
      $size = 'lawfirm_img_medium';
      $thumb = $image['sizes'][ $size ];
      ?>

      <div>
          <a href="<?= $link_url ?>" target="<?= $target ?>">
              <img src="<?= esc_url($thumb); ?>" alt="<?= esc_attr($alt); ?>" />
          </a>
          <h3><?= $headline ?></h3>
          <p class='copy'><?= $copy ?></p>
          <a href="<?= $link_url ?>" class="btn-green-sm" target="<?= $target ?>"><?= $link_title ?></a>
      </div>

          
      <?php endwhile;
  endif; ?>

</div>


<?php endif; ?>