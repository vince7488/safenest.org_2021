<?php
/**
* The template for displaying the footer.
*
* Contains the closing of the #content div and all content after.
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package lawfirm
*/

?>
    <footer class="global-footer" data-ajaxurl="<?php echo admin_url('admin-ajax.php'); ?>">

      <section class="footer-callout">
        <div class="wrap">

          <?php 
          $left_callout = get_field('footer_callout_left_callout', 330); 
          $hide_left = $left_callout['hide'];
          if(!$hide_left){
          ?>
          <div class="chat">
            <img src="<?= get_template_directory_uri() ?>/src/icons/chat.svg" alt="chat with is live">
            <span>
              <h5><?= $left_callout['headline']; ?></h5>
              <p><?= $left_callout['copy']; ?></p>
              <a class="btn-green-sm" href="<?= $left_callout['link']['url']; ?>"><?= $left_callout['link']['title']; ?></a>
            </span>
          </div>
          <?php 
          }


          $right_callout = get_field('footer_callout_right_callout', 330); 
          $hide_right = $right_callout['hide'];
          if(!$hide_right){
          ?>
          <div class="faq">
            <img src="<?= get_template_directory_uri() ?>/src/icons/faq.svg" alt="frequently asked questions">
            <span>
              <h5><?= $right_callout['headline']; ?></h5>
              <p><?= $right_callout['copy']; ?></p>
              <a class="btn-green-sm" href="<?= $right_callout['link']['url']; ?>"><?= $right_callout['link']['title']; ?></a>
            </span>
          </div>
          <?php 
          }
          
          
          $newsletter = get_field('footer_callout_newsletter', 330); ?>
          <div class="subscribe">
          <h5><?= $newsletter['heading'] ?></h5>
          
            <?php gravity_form( '1', false, false , false, null, true, '1', $echo = true ); ?>

          
          <p><?= $newsletter['copy'] ?></p>
          </div>

        </div>
      </section>



      <section class="footer-nav wrap">
      <?php 
        if( have_rows('footer_navigation', 330) ):
          while( have_rows('footer_navigation', 330) ) : the_row();
            $title_link = get_sub_field('column_title');
            $title_url = $title_link['url'];
            $title_title = $title_link['title'];
            $title_target = $title_link['target'] ? $title_link['target'] : '_self';
          ?>
            <span>
              <ul>
              <?php if($title_url == "#" || $title_url == ""){?>
                <li><?= $title_title; ?>
              <?php } else { ?>
                <a href="<?= esc_url( $title_url ); ?>" target="<?= esc_attr( $title_target ); ?>"><li><?= $title_title; ?></li></a>
              <?php }

                // link list in the col
                if( have_rows('column_links') ) :
                  while( have_rows('column_links') ) : the_row(); 
                  
                  $nav_link = get_sub_field('link');
                  $link_url = $nav_link['url'];
                  $link_title = $nav_link['title'];
                  $link_target = $nav_link['target'] ? $nav_link['target'] : '_self';
                  ?>
                    <li><a href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>"><?= esc_html( $link_title ); ?></a></li>
                  <?php endwhile; ?>
                <?php endif; ?>
              </ul>
            </span>
          <?php 
        // end of column repeater
        endwhile; ?>
        <?php endif; ?>
       
      

        <span class="contact">
          <?php 
          $hotline_num = get_field('contact_social_option_hotline_number', 'option');
          $hotline_link = preg_replace("/[^A-Za-z0-9]/", '', $hotline_num);

          $office_num = get_field('contact_social_option_office_number', 'option');
          $office_link = preg_replace("/[^A-Za-z0-9]/", '', $office_num);

          $email = get_field('contact_social_option_email', 'option');

          $tax_id_num = get_field('contact_social_option_tax_id', 'option');
          ?>
          <ul>
            <!-- <li class="heading">Contact</li> -->
            <li>
              <p><strong>24/7 Hotline:</strong></p>
              <a href="tel:<?= $hotline_link ?>"><?= $hotline_num ?></a>
            </li>
            <li>
              <p><strong>Main Office:</strong></p>
              <a href="tel:<?= $office_link ?>"><?= $office_num ?></a>
              <a href="mailto:<?= $email ?>"><?= $email ?></a>
            </li>
            <li>
              <p><strong>Tax ID#</strong><?= $tax_id_num ?></p>
            </li>
          </ul>

          <?php 
          $facebook_link = get_field('contact_social_option_facebook', 'option');
          $twitter_link = get_field('contact_social_option_twitter', 'option');
          $instagram_link = get_field('contact_social_option_instagram', 'option');
          ?>
          <ul class="social">
            <li><a href="<?= $facebook_link ?>" target="_blank"><img src="<?= get_template_directory_uri() ?>/src/icons/facebook.svg" alt="facebook icon"></a></li>
            <li><a href="<?= $twitter_link ?>" target="_blank"><img src="<?= get_template_directory_uri() ?>/src/icons/twitter.svg" alt="twitter icon"></a></li>
            <li><a href="<?= $instagram_link ?>" target="_blank"><img src="<?= get_template_directory_uri() ?>/src/icons/instagram.svg" alt="instagram icon"></a></li>
            <li><a href="mailto:<?= $email ?>" target="_blank"><img src="<?= get_template_directory_uri() ?>/src/icons/email.svg" alt="email icon"></a></li>
          </ul>
          <!-- <a class="btn-green-sm" href="#">Translate</a> -->
          <p>Translate</p>
          <?php echo do_shortcode('[gtranslate]'); ?>
        </span>

      </section>



      <section class="footer-tag">
        <div class="wrap">
          <span>
            <p>© Copyright <?= date("Y"); ?></p>
            <p>All rights reserved.</p>
            <?php 
            $pp_link = get_field('footer_tag_privacy_policy_link', 330);
            $pp_link_url = $pp_link['url'];
            $pp_link_title = $pp_link['title'];
            $pp_link_target = $pp_link['target'] ? $pp_link['target'] : '_self';
            ?>
            <a href="<?= esc_url( $pp_link_url ); ?>" target="<?= esc_attr( $pp_link_target ); ?>"><?= esc_html( $pp_link_title ); ?></a>
          </span>
          <span class="disclaimer">
            <p><?= the_field('footer_tag_tag_copy', 330) ?></p>
          </span>
      </div>
      </section>

    
    </footer>






    </div><!-- #page -->
    <?php wp_footer(); 

    // add Google Anlaytics if we are on the production site
    if ($_SERVER['HTTPS_HOST']==="safenest.org" || $_SERVER['HTTP_HOST']==="safenest.org") { 
        echo "<script>
                window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
                ga('create','UA-123137326-2','auto');ga('send','pageview')
              </script>
              <script src='https://www.google-analytics.com/analytics.js' async defer></script>";
    } ?>

  </body>
</html>


