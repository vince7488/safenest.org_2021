<?php
/**
* The theme head
* imported into header.php which is pulled into all theme pages
*
*
* @package lawfirm
*/

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="bdhwk">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="icon" type="image/png" href="<?= get_template_directory_uri() ?>/favicon.png"/>
<link rel="apple-touch-icon" href="<?= get_template_directory_uri() ?>/apple-touch-icon.png">

<meta property="og:type" content="website" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:url" content="" />
<meta property="og:site_name" content="" />
<meta property="og:image" content="" />
<meta property="og:locale" content="en_US" />

<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Work+Sans:wght@200;300;700&display=swap" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>



<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>