<?php
/**
* The header for our theme.
*
* This is the template that displays all of the <head> section and everything up until <div id="content">
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package lawfirm
*/

?>
<?php get_template_part( 'head' ); ?>

<body <?php body_class(); ?>>
  <div id="page" class="site">


  <?php 
  // only display the animated header if we are on the home page
  // otherwise show the compressed header
  $page_name = get_the_title();
  $compressed_class = "";
  if ($page_name != "Home"){
    $expanded_class = "compressed";
  }


  $hotline_num = get_field('contact_social_option_hotline_number', 'option');
  $hotline_link = preg_replace("/[^A-Za-z0-9]/", '', $hotline_num);


  $link_1 = get_field('header_link_1', 325);
  if($link_1){
    $link_url_1 = $link_1['url'];
    $link_title_1 = $link_1['title'];
    $link_target_1 = $link_1['target'] ? $link_1['target'] : '_self';
  }
  $link_2 = get_field('header_link_2', 325);
  if($link_2){
    $link_url_2 = $link_2['url'];
    $link_title_2 = $link_2['title'];
    $link_target_2 = $link_2['target'] ? $link_2['target'] : '_self';
    }
  ?>  
  <header class="global-header <?= $expanded_class ?>">

    <div class="green-callout">
      <div>
        <p>
          <?php if ($hotline_link && $hotline_num) { ?>
          <strong>24/7 Hotline</strong>
          <a href="tel:<?= $hotline_link ?>" title="hotline"><?= $hotline_num ?></a>
          <?php } ?>
          
          <?php if($link_1){ ?>
            <strong class="mobile-link">|</strong> <a class="mobile-link" href="<?= esc_url( $link_url_1 ); ?>" target="<?= esc_attr( $link_target_1 ); ?>" title="<?= esc_html( $link_title_1 ); ?>"><?= esc_html( $link_title_1 ); ?></a>
          <?php 
          }
          if($link_2){ ?>
            <strong class="mobile-link">|</strong> <a class="mobile-link" href="<?= esc_url( $link_url_2 ); ?>" target="<?= esc_attr( $link_target_2 ); ?>" title="<?= esc_html( $link_title_2 ); ?>"><?= esc_html( $link_title_2 ); ?></a>
          <?php } ?>
        </p>
        <?php if($link_1){ ?>
          <p class="btn-callout"><a href="<?= esc_url( $link_url_1 ); ?>" target="<?= esc_attr( $link_target_1 ); ?>">
            <strong><?= esc_html( $link_title_1 ); ?></strong>
          </a></p>
          <?php }
          if($link_2){ ?>
            <p class="btn-callout"><a href="<?= esc_url( $link_url_2 ); ?>" target="<?= esc_attr( $link_target_2 ); ?>">
              <strong><?= esc_html( $link_title_2 ); ?></strong>
            </a></p>
          <?php } ?>
          <a class="search-icon"><?php echo file_get_contents(get_template_directory_uri() . "/src/icons/search.svg", false, stream_context_create($arrContextOptions)); ?></a>
      </div>
    </div>

    <div class="logo">
      <a href="/"><img class="white" src="<?= get_template_directory_uri() ?>/src/img/logo-white.svg" alt="SafeNest Logo"></a>
      <a href="/"><img class="color" src="<?= get_template_directory_uri() ?>/src/img/logo-color.svg" alt="SafeNest Logo"></a>
    </div>

    <div class="navigation">
      <div class="action"> 
        <p><strong>24/7 Hotline</strong><a href="tel:<?= $hotline_link ?>"><?= $hotline_num ?></a></p>
        <?php if($link_1){ ?>
          <a class="btn-green-sm" href="<?= esc_url( $link_url_1 ); ?>" target="<?= esc_attr( $link_target_1 ); ?>"><?= esc_html( $link_title_1 ); ?></a>
        <?php 
        }
        if($link_2){ ?>
          <a class="btn-green-sm" href="<?= esc_url( $link_url_2 ); ?>" target="<?= esc_attr( $link_target_2 ); ?>"><?= esc_html( $link_title_2 ); ?></a>
        <?php } ?>
        <a class="search-icon"><?php echo file_get_contents(get_template_directory_uri() . "/src/icons/search.svg", false, stream_context_create($arrContextOptions)); ?></a>
      </div>





      <div class="nav-desk">
          <nav>
            <?php 
            if( have_rows('main_navigation', 325) ):
              while( have_rows('main_navigation', 325) ) : the_row();
              
                $nav_link = get_sub_field('nav_item');
                $link_url = $nav_link['url'];
                $link_title = $nav_link['title'];
                $link_target = $nav_link['target'] ? $nav_link['target'] : '_self';
              ?>
                <div class="nav-box">
                  <div class="top-link">
                    <a class="top-link" href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>"><?= esc_html( $link_title ); ?></a>
                    <?php 
                    //let's check if there's a sub menu
                    $is_sub = get_sub_field('sub_nav');
                    if($is_sub){?>
                      <?php echo file_get_contents(get_template_directory_uri() . "/src/icons/arrow-nav.svg", false, stream_context_create($arrContextOptions)); ?>
                    <?php } ?>
                  </div>
                  

                  <?php if($is_sub){?>
                  <div class="nav-drop">
                  <?php 
                    if( have_rows('sub_nav') ):
                      while( have_rows('sub_nav') ) : the_row(); 
                      $sub_nav_link = get_sub_field('sub_nav_item');
                      $sub_link_url = $sub_nav_link['url'];
                      $sub_link_title = $sub_nav_link['title'];
                      $sub_link_target = $sub_nav_link['target'] ? $sub_nav_link['target'] : '_self';
                      ?>
                        <a href="<?= esc_url( $sub_link_url ); ?>" target="<?= esc_attr( $sub_link_target ); ?>"><?= esc_html( $sub_link_title ); ?></a>
                      <?php endwhile;
                    endif;
                  ?>
                  </div>
                  <?php } ?>
                  
                </div>
              <?php endwhile; ?>
            <?php endif; 
            
            $main_nav_btn = get_field('main_nav_button' , 325);
            if($main_nav_btn){
              $main_nav_btn_url = $main_nav_btn['url'];
              $main_nav_btn_title = $main_nav_btn['title'];
              $main_nav_btn_target = $main_nav_btn['target'] ? $main_nav_btn['target'] : '_self';
            ?>
              <a class="btn-green-sm" href="<?= esc_url( $main_nav_btn_url ); ?>" target="<?= esc_attr( $main_nav_btn_target ); ?>"><?= esc_html( $main_nav_btn_title ); ?></a>
            <?php } ?>
          </nav>
      </div>





      <div class="ham">
        <div class="lines">
          <span></span>
          <span></span>
          <span></span>
        </div>
        <div class="center1"></div>
        <div class="center2"></div>
      </div>





      <div class="nav-mobile">
        <div class="scroll-box">
          <nav>
            <?php 
            if( have_rows('main_navigation', 325) ):
              while( have_rows('main_navigation', 325) ) : the_row();
              
                $nav_link = get_sub_field('nav_item');
                $link_url = $nav_link['url'];
                $link_title = $nav_link['title'];
                $link_target = $nav_link['target'] ? $nav_link['target'] : '_self';
              ?>
              <div class="nav-accord">
                <div class="main">
                  <a href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>"><?= esc_html( $link_title ); ?></a>

                  <?php 
                  //let's check if there's a sub menu
                  $is_sub = get_sub_field('sub_nav');
                  if($is_sub){?>
                    <span></span>
                  <?php } ?>
                </div>
                <div class="subs">
                <?php 
                  if( have_rows('sub_nav') ):
                    while( have_rows('sub_nav') ) : the_row(); 
                    $sub_nav_link = get_sub_field('sub_nav_item');
                    $sub_link_url = $sub_nav_link['url'];
                    $sub_link_title = $sub_nav_link['title'];
                    $sub_link_target = $sub_nav_link['target'] ? $sub_nav_link['target'] : '_self';
                    ?>
                      <a href="<?= esc_url( $sub_link_url ); ?>" target="<?= esc_attr( $sub_link_target ); ?>"><?= esc_html( $sub_link_title ); ?></a>
                    <?php endwhile;
                  endif;
                ?>
                </div>
              </div>
              <?php endwhile; ?>
            <?php endif; ?>
          </nav>
          <div class="buttons">
          <?php if($link_1){ ?>
            <a class="btn-green-sm" href="<?= esc_url( $link_url_1 ); ?>" target="<?= esc_attr( $link_target_1 ); ?>"><?= esc_html( $link_title_1 ); ?></a>
          <?php 
          }
          if($link_2){ ?>
            <a class="btn-green-sm" href="<?= esc_url( $link_url_2 ); ?>" target="<?= esc_attr( $link_target_2 ); ?>"><?= esc_html( $link_title_2 ); ?></a>
          <?php } ?>
          </div>
          <div class="bottom">
            <span>
              <a href="/resources/faqs/">
                <img src="<?= get_template_directory_uri() ?>/src/icons/chat.svg" alt="chat with is live">
                <p>Chat</p>
              </a>
            </span>
            <span>
              <a href="/resources/faqs/">
                <img src="<?= get_template_directory_uri() ?>/src/icons/faq.svg" alt="frequently asked questions">
                <p>FAQs</p>
              </a>
            </span>
            <span id="search-icon-mobile">
              <?php echo file_get_contents(get_template_directory_uri() . "/src/icons/search.svg", false, stream_context_create($arrContextOptions)); ?>
              <p>Search</p>
            </span>
          </div>
          </div>
      </div>
      
    </div>

  </header>


  <?php get_template_part( 'search-overlay' ); ?>