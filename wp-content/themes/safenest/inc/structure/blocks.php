<?php
/**
 * Include Advanced Custom Fields within theme
 *
 * @link http://www.advancedcustomfields.com/resources/including-acf-in-a-plugin-theme/
 * @package lawfirm
 */

function my_acf_block_render_callback( $block ) {
  
  // convert name ("acf/block-name") into path friendly slug ("block-name")
  $slug = str_replace('acf/', '', $block['name']);
  // include a template part from within the "template-parts/blocks" folder
  if( file_exists( get_theme_file_path("/blocks/content-{$slug}.php") ) ) {
    include( get_theme_file_path("/blocks/content-{$slug}.php") );
  }
}

//create custom block categories
function my_plugin_block_categories( $categories, $post ) {
  // if ( $post->post_type !== 'post' ) {
  //     return $categories;
  // }
  return array_merge(
      $categories,
      array(
          array(
              'slug' => 'block-home',
              'title' => __( 'Home', 'block-home' ),
              'icon'  => 'admin-home',
          ),
          array(
            'slug' => 'block-basic',
            'title' => __( 'Basic', 'block-basic' ),
            'icon'  => 'edit',
          ),
          array(
            'slug' => 'block-layout',
            'title' => __( 'Layout', 'block-layout' ),
            'icon'  => 'welcome-widgets-menus',
          ),
      )
  );
}
add_filter( 'block_categories', 'my_plugin_block_categories', 10, 2 );



//add only blocks that are needed
function acf_allowed_block_types( $allowed_blocks ) {

  //all default block types set
  $blocks = array(
    // 'acf/basic-headline',
    // 'acf/basic-text',
    // 'acf/basic-list',
    // 'acf/basic-wysiwyg',
    'acf/image-stack',
    'acf/gallery-slider',
    'acf/image-text-callout',
    'acf/testimonial-slider',
    'acf/image-with-text',
    'acf/number-callout',
    'acf/recent-posts',
    'acf/footer-callout',
    'acf/text-2-col-list',
    'acf/text-with-button',
    'acf/divider-text-callout',
    'acf/divider-contact',
    'acf/sponsors-list',
    'acf/history-slider',
    'acf/tabbed-image-with-text',
    'acf/accordion-link-list',
    'acf/text-builder',
    'acf/leadership-list',
    'acf/location-list',
    'acf/events-list',
    'acf/hubspot',
    'acf/three-col-cta',
    'acf/gravity-form'
  );


  $page_name = get_the_title();

  //home only block types
  if( $page_name == "Home" ) {
      array_push(
        $blocks,
        'acf/home-hero'
      );
  }

  //internal pages block types
  if( $page_name != "Home" ) {
    array_push(
      $blocks,
      'acf/internal-hero'
    );
  }

  //news page block types
  if( $page_name == "News" ) {
    array_push(
      $blocks,
      'acf/news-stories'
    );
  }
 
  return $blocks; 

}
add_filter( 'allowed_block_types', 'acf_allowed_block_types' );



//block registration
add_action('acf/init', 'my_acf_init');
function my_acf_init() {
  
  // check function exists
  if( function_exists('acf_register_block') ) {


    //--------------------------------
    // BASIC BLOCKS
    //--------------------------------

    // register a basic headline block
    acf_register_block(array(
      'name'        => 'basic-headline',
      'title'       => __('Simple Headline'),
      'description'   => __('a simple headline element'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-basic',
      'mode' => 'edit',
      'icon'        => '<svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M448 96v320h32a16 16 0 0 1 16 16v32a16 16 0 0 1-16 16H320a16 16 0 0 1-16-16v-32a16 16 0 0 1 16-16h32V288H160v128h32a16 16 0 0 1 16 16v32a16 16 0 0 1-16 16H32a16 16 0 0 1-16-16v-32a16 16 0 0 1 16-16h32V96H32a16 16 0 0 1-16-16V48a16 16 0 0 1 16-16h160a16 16 0 0 1 16 16v32a16 16 0 0 1-16 16h-32v128h192V96h-32a16 16 0 0 1-16-16V48a16 16 0 0 1 16-16h160a16 16 0 0 1 16 16v32a16 16 0 0 1-16 16z"></path></svg>',
      'keywords'      => array( 'basic', 'headline'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register a basic text block
    acf_register_block(array(
      'name'        => 'basic-text',
      'title'       => __('Simple Text'),
      'description'   => __('a simple text block element'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-basic',
      'mode' => 'edit',
      'icon'        => '<svg aria-hidden="true" viewBox="0 0 640 512"><path fill="currentColor" d="M624 32H272a16 16 0 0 0-16 16v96a16 16 0 0 0 16 16h32a16 16 0 0 0 16-16v-32h88v304h-40a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h160a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16h-40V112h88v32a16 16 0 0 0 16 16h32a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16zM304 224H16a16 16 0 0 0-16 16v64a16 16 0 0 0 16 16h32a16 16 0 0 0 16-16v-16h56v128H96a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h128a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16h-24V288h56v16a16 16 0 0 0 16 16h32a16 16 0 0 0 16-16v-64a16 16 0 0 0-16-16z"></path></svg>',
      'keywords'      => array( 'basic', 'text'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register a basic text list block
    acf_register_block(array(
      'name'        => 'basic-list',
      'title'       => __('Simple List'),
      'description'   => __('a simple bullet list element'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-basic',
      'mode' => 'edit',
      'icon'        => '<svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M80 368H16a16 16 0 0 0-16 16v64a16 16 0 0 0 16 16h64a16 16 0 0 0 16-16v-64a16 16 0 0 0-16-16zm0-320H16A16 16 0 0 0 0 64v64a16 16 0 0 0 16 16h64a16 16 0 0 0 16-16V64a16 16 0 0 0-16-16zm0 160H16a16 16 0 0 0-16 16v64a16 16 0 0 0 16 16h64a16 16 0 0 0 16-16v-64a16 16 0 0 0-16-16zm416 176H176a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h320a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16zm0-320H176a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h320a16 16 0 0 0 16-16V80a16 16 0 0 0-16-16zm0 160H176a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h320a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16z"></path></svg>',
      'keywords'      => array( 'basic', 'list'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register a basic Wysiwyg editor
    acf_register_block(array(
      'name'        => 'basic-wysiwyg',
      'title'       => __('Wysiwyg Editor'),
      'description'   => __('a simple Wysiwyg element'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-basic',
      'mode' => 'edit',
      'icon'        => '<svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M496 160h-16V16a16 16 0 0 0-16-16h-48a16 16 0 0 0-14.29 8.83l-16 32A16 16 0 0 0 400 64h16v96h-16a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h96a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16zM336 64h-67a16 16 0 0 0-13.14 6.87l-79.9 115-79.9-115A16 16 0 0 0 83 64H16A16 16 0 0 0 0 80v48a16 16 0 0 0 16 16h33.48l77.81 112-77.81 112H16a16 16 0 0 0-16 16v48a16 16 0 0 0 16 16h67a16 16 0 0 0 13.14-6.87l79.9-115 79.9 115A16 16 0 0 0 269 448h67a16 16 0 0 0 16-16v-48a16 16 0 0 0-16-16h-33.48l-77.81-112 77.81-112H336a16 16 0 0 0 16-16V80a16 16 0 0 0-16-16z"></path></svg>',
      'keywords'      => array( 'basic', 'wysiwyg'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));


    //--------------------------------
    // CUSTOM LAYOUT BLOCKS
    //--------------------------------

    // register an Image Stack block
    acf_register_block(array(
      'name'        => 'image-stack',
      'title'       => __('Image Stack'),
      'description'   => __('a stack of images with text'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M512 256.01c0-12.86-7.53-24.42-19.12-29.44l-79.69-34.58 79.66-34.57c11.62-5.03 19.16-16.59 19.16-29.44s-7.53-24.41-19.12-29.42L274.66 3.89c-11.84-5.17-25.44-5.19-37.28-.02L19.16 98.55C7.53 103.58 0 115.14 0 127.99s7.53 24.41 19.12 29.42l79.7 34.58-79.67 34.57C7.53 231.58 0 243.15 0 256.01c0 12.84 7.53 24.41 19.12 29.42L98.81 320l-79.65 34.56C7.53 359.59 0 371.15 0 384.01c0 12.84 7.53 24.41 19.12 29.42l218.28 94.69a46.488 46.488 0 0 0 18.59 3.88c6.34-.02 12.69-1.3 18.59-3.86l218.25-94.69c11.62-5.03 19.16-16.59 19.16-29.44 0-12.86-7.53-24.42-19.12-29.44L413.19 320l79.65-34.56c11.63-5.03 19.16-16.59 19.16-29.43zM255.47 47.89l.03.02h-.06l.03-.02zm.53.23l184.16 79.89-183.63 80.09-184.62-80.11L256 48.12zm184.19 335.92l-183.66 80.07L71.91 384l87.21-37.84 78.29 33.96A46.488 46.488 0 0 0 256 384c6.34-.02 12.69-1.3 18.59-3.86l78.29-33.97 87.31 37.87zM256.53 336.1L71.91 255.99l87.22-37.84 78.28 33.96a46.488 46.488 0 0 0 18.59 3.88c6.34-.02 12.69-1.3 18.59-3.86l78.29-33.97 87.31 37.88-183.66 80.06z"></path></svg>',
      'keywords'      => array( 'layout', 'image', 'stack'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register a gallery block
    acf_register_block(array(
      'name'        => 'gallery-slider',
      'title'       => __('Gallery Slider'),
      'description'   => __('a full width slider'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg aria-hidden="true" viewBox="0 0 576 512"><path fill="currentColor" d="M480 416v16c0 26.51-21.49 48-48 48H48c-26.51 0-48-21.49-48-48V176c0-26.51 21.49-48 48-48h16v208c0 44.112 35.888 80 80 80h336zm96-80V80c0-26.51-21.49-48-48-48H144c-26.51 0-48 21.49-48 48v256c0 26.51 21.49 48 48 48h384c26.51 0 48-21.49 48-48zM256 128c0 26.51-21.49 48-48 48s-48-21.49-48-48 21.49-48 48-48 48 21.49 48 48zm-96 144l55.515-55.515c4.686-4.686 12.284-4.686 16.971 0L272 256l135.515-135.515c4.686-4.686 12.284-4.686 16.971 0L512 208v112H160v-48z"></path></svg>',
      'keywords'      => array( 'layout', 'gallery', 'slider'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));


    // register a image with text callout
    acf_register_block(array(
      'name'        => 'image-text-callout',
      'title'       => __('Image w/ Text Callout'),
      'description'   => __('an image that includes a text overlay and text on the side'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg aria-hidden="true" viewBox="0 0 576 512"><path fill="currentColor" d="M528 32H48C21.5 32 0 53.5 0 80v16h576V80c0-26.5-21.5-48-48-48zM0 432c0 26.5 21.5 48 48 48h480c26.5 0 48-21.5 48-48V128H0v304zm352-232c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8H360c-4.4 0-8-3.6-8-8v-16zm0 64c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8H360c-4.4 0-8-3.6-8-8v-16zm0 64c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8H360c-4.4 0-8-3.6-8-8v-16zM176 192c35.3 0 64 28.7 64 64s-28.7 64-64 64-64-28.7-64-64 28.7-64 64-64zM67.1 396.2C75.5 370.5 99.6 352 128 352h8.2c12.3 5.1 25.7 8 39.8 8s27.6-2.9 39.8-8h8.2c28.4 0 52.5 18.5 60.9 44.2 3.2 9.9-5.2 19.8-15.6 19.8H82.7c-10.4 0-18.8-10-15.6-19.8z"></path></svg>',
      'keywords'      => array( 'image', 'text', 'callout'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register a testimonial slider
    acf_register_block(array(
      'name'        => 'testimonial-slider',
      'title'       => __('Testimonial Slider'),
      'description'   => __('A slider that displays testimonials'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M464 32H336c-26.5 0-48 21.5-48 48v128c0 26.5 21.5 48 48 48h80v64c0 35.3-28.7 64-64 64h-8c-13.3 0-24 10.7-24 24v48c0 13.3 10.7 24 24 24h8c88.4 0 160-71.6 160-160V80c0-26.5-21.5-48-48-48zm-288 0H48C21.5 32 0 53.5 0 80v128c0 26.5 21.5 48 48 48h80v64c0 35.3-28.7 64-64 64h-8c-13.3 0-24 10.7-24 24v48c0 13.3 10.7 24 24 24h8c88.4 0 160-71.6 160-160V80c0-26.5-21.5-48-48-48z"></path></svg>',
      'keywords'      => array( 'testimonal', 'slider', 'gallery'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register an internal hero
    acf_register_block(array(
      'name'        => 'internal-hero',
      'title'       => __('Internal Page Hero'),
      'description'   => __('the hero that should be used on all internal pages'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg aria-hidden="true" viewBox="0 0 640 512"><path fill="#616161" d="M320.67 64c-442.6 0-357.57 384-158.46 384 39.9 0 77.47-20.69 101.42-55.86l25.73-37.79c15.66-22.99 46.97-22.99 62.63 0l25.73 37.79C401.66 427.31 439.23 448 479.13 448c189.86 0 290.63-384-158.46-384zM184 308.36c-41.06 0-67.76-25.66-80.08-41.05-5.23-6.53-5.23-16.09 0-22.63 12.32-15.4 39.01-41.05 80.08-41.05s67.76 25.66 80.08 41.05c5.23 6.53 5.23 16.09 0 22.63-12.32 15.4-39.02 41.05-80.08 41.05zm272 0c-41.06 0-67.76-25.66-80.08-41.05-5.23-6.53-5.23-16.09 0-22.63 12.32-15.4 39.01-41.05 80.08-41.05s67.76 25.66 80.08 41.05c5.23 6.53 5.23 16.09 0 22.63-12.32 15.4-39.02 41.05-80.08 41.05z"></path></svg>',
      'keywords'      => array( 'internal', 'hero', 'images'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register a image with a text block
    acf_register_block(array(
      'name'        => 'image-with-text',
      'title'       => __('Image w/ Text'),
      'description'   => __('an image that includes text on the side'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg aria-hidden="true" viewBox="0 0 576 512"><path fill="currentColor" d="M528 32H48C21.5 32 0 53.5 0 80v16h576V80c0-26.5-21.5-48-48-48zM0 432c0 26.5 21.5 48 48 48h480c26.5 0 48-21.5 48-48V128H0v304zm352-232c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8H360c-4.4 0-8-3.6-8-8v-16zm0 64c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8H360c-4.4 0-8-3.6-8-8v-16zm0 64c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8H360c-4.4 0-8-3.6-8-8v-16zM176 192c35.3 0 64 28.7 64 64s-28.7 64-64 64-64-28.7-64-64 28.7-64 64-64zM67.1 396.2C75.5 370.5 99.6 352 128 352h8.2c12.3 5.1 25.7 8 39.8 8s27.6-2.9 39.8-8h8.2c28.4 0 52.5 18.5 60.9 44.2 3.2 9.9-5.2 19.8-15.6 19.8H82.7c-10.4 0-18.8-10-15.6-19.8z"></path></svg>',
      'keywords'      => array( 'image', 'text'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register a number callout block
    acf_register_block(array(
      'name'        => 'number-callout',
      'title'       => __('Number Callout'),
      'description'   => __('a full page callout with numbers! yeah!'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg viewBox="0 0 448 512"><path fill="currentColor" d="M107.31 36.69a16 16 0 0 0-22.62 0l-80 96C-5.35 142.74 1.78 160 16 160h48v304a16 16 0 0 0 16 16h32a16 16 0 0 0 16-16V160h48c14.21 0 21.38-17.24 11.31-27.31zM400 416h-16V304a16 16 0 0 0-16-16h-48a16 16 0 0 0-14.29 8.83l-16 32A16 16 0 0 0 304 352h16v64h-16a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h96a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16zM330.17 34.91a79 79 0 0 0-55 54.17c-14.27 51.05 21.19 97.77 68.83 102.53a84.07 84.07 0 0 1-20.85 12.91c-7.57 3.4-10.8 12.47-8.18 20.34l9.9 20c2.87 8.63 12.53 13.49 20.9 9.91 58-24.77 86.25-61.61 86.25-132V112c-.02-51.21-48.4-91.34-101.85-77.09zM352 132a20 20 0 1 1 20-20 20 20 0 0 1-20 20z"></path></svg>',
      'keywords'      => array( 'numbers', 'callout'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

     // register a recent posts block
     acf_register_block(array(
      'name'        => 'recent-posts',
      'title'       => __('Recent Posts'),
      'description'   => __('a set of recent posts'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg viewBox="0 0 576 512"><path fill="currentColor" d="M552 64H112c-20.858 0-38.643 13.377-45.248 32H24c-13.255 0-24 10.745-24 24v272c0 30.928 25.072 56 56 56h496c13.255 0 24-10.745 24-24V88c0-13.255-10.745-24-24-24zM48 392V144h16v248c0 4.411-3.589 8-8 8s-8-3.589-8-8zm480 8H111.422c.374-2.614.578-5.283.578-8V112h416v288zM172 280h136c6.627 0 12-5.373 12-12v-96c0-6.627-5.373-12-12-12H172c-6.627 0-12 5.373-12 12v96c0 6.627 5.373 12 12 12zm28-80h80v40h-80v-40zm-40 140v-24c0-6.627 5.373-12 12-12h136c6.627 0 12 5.373 12 12v24c0 6.627-5.373 12-12 12H172c-6.627 0-12-5.373-12-12zm192 0v-24c0-6.627 5.373-12 12-12h104c6.627 0 12 5.373 12 12v24c0 6.627-5.373 12-12 12H364c-6.627 0-12-5.373-12-12zm0-144v-24c0-6.627 5.373-12 12-12h104c6.627 0 12 5.373 12 12v24c0 6.627-5.373 12-12 12H364c-6.627 0-12-5.373-12-12zm0 72v-24c0-6.627 5.373-12 12-12h104c6.627 0 12 5.373 12 12v24c0 6.627-5.373 12-12 12H364c-6.627 0-12-5.373-12-12z"></path></svg>',
      'keywords'      => array( 'recent', 'posts', 'news'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register a recent posts block
    acf_register_block(array(
      'name'        => 'footer-callout',
      'title'       => __('Footer Callout'),
      'description'   => __('a set of image links that are usually placed before the footer'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg aria-hidden="true" viewBox="0 0 640 512"><path fill="currentColor" d="M192 160h32V32h-32c-35.35 0-64 28.65-64 64s28.65 64 64 64zM0 416c0 35.35 28.65 64 64 64h32V352H64c-35.35 0-64 28.65-64 64zm337.46-128c-34.91 0-76.16 13.12-104.73 32-24.79 16.38-44.52 32-104.73 32v128l57.53 15.97c26.21 7.28 53.01 13.12 80.31 15.05 32.69 2.31 65.6.67 97.58-6.2C472.9 481.3 512 429.22 512 384c0-64-84.18-96-174.54-96zM491.42 7.19C459.44.32 426.53-1.33 393.84.99c-27.3 1.93-54.1 7.77-80.31 15.04L256 32v128c60.2 0 79.94 15.62 104.73 32 28.57 18.88 69.82 32 104.73 32C555.82 224 640 192 640 128c0-45.22-39.1-97.3-148.58-120.81z"></path></svg>',
      'keywords'      => array( 'footer', 'callout', 'images', 'green'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register a text 2 col list
    acf_register_block(array(
      'name'        => 'text-2-col-list',
      'title'       => __('Text 2 Col List'),
      'description'   => __('a list of text items that are displayed in two columns'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg viewBox="0 0 512 512"><path fill="currentColor" d="M464 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V80c0-26.51-21.49-48-48-48zM224 416H64V160h160v256zm224 0H288V160h160v256z"></path></svg>',
      'keywords'      => array( 'text', 'list'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register a text 2 col list
    acf_register_block(array(
      'name'        => 'text-2-col-list',
      'title'       => __('Text 2 Col List'),
      'description'   => __('a list of text items that are displayed in two columns'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg viewBox="0 0 512 512"><path fill="currentColor" d="M464 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V80c0-26.51-21.49-48-48-48zM224 416H64V160h160v256zm224 0H288V160h160v256z"></path></svg>',
      'keywords'      => array( 'text', 'list'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

     // register a text block with a button
     acf_register_block(array(
      'name'        => 'text-with-button',
      'title'       => __('Text with a Button'),
      'description'   => __('a single or double column text area with a link'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg viewBox="0 0 512 512"><path fill="currentColor" d="M326.612 185.391c59.747 59.809 58.927 155.698.36 214.59-.11.12-.24.25-.36.37l-67.2 67.2c-59.27 59.27-155.699 59.262-214.96 0-59.27-59.26-59.27-155.7 0-214.96l37.106-37.106c9.84-9.84 26.786-3.3 27.294 10.606.648 17.722 3.826 35.527 9.69 52.721 1.986 5.822.567 12.262-3.783 16.612l-13.087 13.087c-28.026 28.026-28.905 73.66-1.155 101.96 28.024 28.579 74.086 28.749 102.325.51l67.2-67.19c28.191-28.191 28.073-73.757 0-101.83-3.701-3.694-7.429-6.564-10.341-8.569a16.037 16.037 0 0 1-6.947-12.606c-.396-10.567 3.348-21.456 11.698-29.806l21.054-21.055c5.521-5.521 14.182-6.199 20.584-1.731a152.482 152.482 0 0 1 20.522 17.197zM467.547 44.449c-59.261-59.262-155.69-59.27-214.96 0l-67.2 67.2c-.12.12-.25.25-.36.37-58.566 58.892-59.387 154.781.36 214.59a152.454 152.454 0 0 0 20.521 17.196c6.402 4.468 15.064 3.789 20.584-1.731l21.054-21.055c8.35-8.35 12.094-19.239 11.698-29.806a16.037 16.037 0 0 0-6.947-12.606c-2.912-2.005-6.64-4.875-10.341-8.569-28.073-28.073-28.191-73.639 0-101.83l67.2-67.19c28.239-28.239 74.3-28.069 102.325.51 27.75 28.3 26.872 73.934-1.155 101.96l-13.087 13.087c-4.35 4.35-5.769 10.79-3.783 16.612 5.864 17.194 9.042 34.999 9.69 52.721.509 13.906 17.454 20.446 27.294 10.606l37.106-37.106c59.271-59.259 59.271-155.699.001-214.959z"></path></svg>',
      'keywords'      => array( 'text', 'button'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register a text/button divder
    acf_register_block(array(
      'name'        => 'divider-text-callout',
      'title'       => __('Divider Text Callout'),
      'description'   => __('a divider element with a purple background'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg viewBox="0 0 448 512"><path fill="currentColor" d="M224 352c-35.35 0-64 28.65-64 64s28.65 64 64 64 64-28.65 64-64-28.65-64-64-64zm0-192c35.35 0 64-28.65 64-64s-28.65-64-64-64-64 28.65-64 64 28.65 64 64 64zm192 48H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg>',
      'keywords'      => array( 'text', 'button', "divider", 'callout'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register a text/button divder
    acf_register_block(array(
      'name'        => 'divider-contact',
      'title'       => __('Divider with Contact Info'),
      'description'   => __('a divider element with contact information'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg viewBox="0 0 448 512"><path fill="currentColor" d="M436 160c6.6 0 12-5.4 12-12v-40c0-6.6-5.4-12-12-12h-20V48c0-26.5-21.5-48-48-48H48C21.5 0 0 21.5 0 48v416c0 26.5 21.5 48 48 48h320c26.5 0 48-21.5 48-48v-48h20c6.6 0 12-5.4 12-12v-40c0-6.6-5.4-12-12-12h-20v-64h20c6.6 0 12-5.4 12-12v-40c0-6.6-5.4-12-12-12h-20v-64h20zm-68 304H48V48h320v416zM208 256c35.3 0 64-28.7 64-64s-28.7-64-64-64-64 28.7-64 64 28.7 64 64 64zm-89.6 128h179.2c12.4 0 22.4-8.6 22.4-19.2v-19.2c0-31.8-30.1-57.6-67.2-57.6-10.8 0-18.7 8-44.8 8-26.9 0-33.4-8-44.8-8-37.1 0-67.2 25.8-67.2 57.6v19.2c0 10.6 10 19.2 22.4 19.2z"></path></svg>',
      'keywords'      => array( 'contact', "divider", 'callout', 'green'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register a sponsors/logo list block
    acf_register_block(array(
      'name'        => 'sponsors-list',
      'title'       => __('Sponsors Logo List'),
      'description'   => __('A List of Sponsors Logo with Text'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg viewBox="0 0 448 512"><path fill="currentColor" d="M96 288H32c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h64c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32zm160 0h-64c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h64c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32zm160 0h-64c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h64c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32zM96 96H32c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h64c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32zm160 0h-64c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h64c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32zm160 0h-64c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h64c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32z"></path></svg>',
      'keywords'      => array( 'sponsors', "logo", 'list'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register a history slider block
    acf_register_block(array(
      'name'        => 'history-slider',
      'title'       => __('History Slider'),
      'description'   => __('A slider that displays history posts'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg viewBox="0 0 512 512"><path fill="currentColor" d="M501.62 92.11L267.24 2.04a31.958 31.958 0 0 0-22.47 0L10.38 92.11A16.001 16.001 0 0 0 0 107.09V144c0 8.84 7.16 16 16 16h480c8.84 0 16-7.16 16-16v-36.91c0-6.67-4.14-12.64-10.38-14.98zM64 192v160H48c-8.84 0-16 7.16-16 16v48h448v-48c0-8.84-7.16-16-16-16h-16V192h-64v160h-96V192h-64v160h-96V192H64zm432 256H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h480c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z"></path></svg>',
      'keywords'      => array( 'history', "slider", 'list'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register a tabbed image with text block
    acf_register_block(array(
      'name'        => 'tabbed-image-with-text',
      'title'       => __('Tabbed Image w/ Text'),
      'description'   => __('a tabbed block with images and text on the side'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg aria-hidden="true" viewBox="0 0 576 512"><path fill="currentColor" d="M528 32H48C21.5 32 0 53.5 0 80v16h576V80c0-26.5-21.5-48-48-48zM0 432c0 26.5 21.5 48 48 48h480c26.5 0 48-21.5 48-48V128H0v304zm352-232c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8H360c-4.4 0-8-3.6-8-8v-16zm0 64c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8H360c-4.4 0-8-3.6-8-8v-16zm0 64c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8H360c-4.4 0-8-3.6-8-8v-16zM176 192c35.3 0 64 28.7 64 64s-28.7 64-64 64-64-28.7-64-64 28.7-64 64-64zM67.1 396.2C75.5 370.5 99.6 352 128 352h8.2c12.3 5.1 25.7 8 39.8 8s27.6-2.9 39.8-8h8.2c28.4 0 52.5 18.5 60.9 44.2 3.2 9.9-5.2 19.8-15.6 19.8H82.7c-10.4 0-18.8-10-15.6-19.8z"></path></svg>',
      'keywords'      => array( 'image', 'text', 'tab', 'tabbed'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register an accordion link list
    acf_register_block(array(
      'name'        => 'accordion-link-list',
      'title'       => __('Accordion Link List'),
      'description'   => __('an accordion with link'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg viewBox="0 0 448 512"><path fill="currentColor" d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path></svg>',
      'keywords'      => array( 'accordion', 'links', 'list', 'tabs'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register a text build block
    acf_register_block(array(
      'name'        => 'text-builder',
      'title'       => __('Text Builder'),
      'description'   => __('section builder for text elements'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg viewBox="0 0 384 512"><path fill="currentColor" d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm64 236c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-64c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-72v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm96-114.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z"></path></svg>',
      'keywords'      => array( 'text', 'block', 'list', 'button'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

     // register a leadership list
     acf_register_block(array(
      'name'        => 'leadership-list',
      'title'       => __('Leadership List'),
      'description'   => __('a list of people with fields for position'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg viewBox="0 0 576 512"><path fill="currentColor" d="M528 64H384v96H192V64H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h480c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM288 224c35.3 0 64 28.7 64 64s-28.7 64-64 64-64-28.7-64-64 28.7-64 64-64zm93.3 224H194.7c-10.4 0-18.8-10-15.6-19.8 8.3-25.6 32.4-44.2 60.9-44.2h8.2c12.3 5.1 25.7 8 39.8 8s27.6-2.9 39.8-8h8.2c28.4 0 52.5 18.5 60.9 44.2 3.2 9.8-5.2 19.8-15.6 19.8zM352 32c0-17.7-14.3-32-32-32h-64c-17.7 0-32 14.3-32 32v96h128V32z"></path></svg>',
      'keywords'      => array( 'text', 'people', 'list', 'leader'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register a location list
    acf_register_block(array(
      'name'        => 'location-list',
      'title'       => __('Location List'),
      'description'   => __('a list of location cards'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg viewBox="0 0 384 512"><path fill="currentColor" d="M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z"></path></svg>',
      'keywords'      => array( 'location', 'contact', 'list', 'address', 'map'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register an events list
    acf_register_block(array(
      'name'        => 'events-list',
      'title'       => __('Events List'),
      'description'   => __('a list of event cards'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg viewBox="0 0 448 512"><path fill="currentColor" d="M148 288h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm108-12v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm96 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm-96 96v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm-96 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm192 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm96-260v352c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h48V12c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v52h128V12c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v52h48c26.5 0 48 21.5 48 48zm-48 346V160H48v298c0 3.3 2.7 6 6 6h340c3.3 0 6-2.7 6-6z"></path></svg>',
      'keywords'      => array( 'events', 'event', 'list'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

    // register a hubspot form
    acf_register_block(array(
      'name'        => 'hubspot',
      'title'       => __('Hubspot'),
      'description'   => __('a block to embed a hubspot form'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg viewBox="0 0 448 512"><path fill="currentColor" d="M448 75.2v361.7c0 24.3-19 43.2-43.2 43.2H43.2C19.3 480 0 461.4 0 436.8V75.2C0 51.1 18.8 32 43.2 32h361.7c24 0 43.1 18.8 43.1 43.2zm-37.3 361.6V75.2c0-3-2.6-5.8-5.8-5.8h-9.3L285.3 144 224 94.1 162.8 144 52.5 69.3h-9.3c-3.2 0-5.8 2.8-5.8 5.8v361.7c0 3 2.6 5.8 5.8 5.8h361.7c3.2.1 5.8-2.7 5.8-5.8zM150.2 186v37H76.7v-37h73.5zm0 74.4v37.3H76.7v-37.3h73.5zm11.1-147.3l54-43.7H96.8l64.5 43.7zm210 72.9v37h-196v-37h196zm0 74.4v37.3h-196v-37.3h196zm-84.6-147.3l64.5-43.7H232.8l53.9 43.7zM371.3 335v37.3h-99.4V335h99.4z"></path></svg>',
      'keywords'      => array( 'hubspot', 'form', 'embed'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));


    // register a 3 col cta
    acf_register_block(array(
      'name'        => 'three-col-cta',
      'title'       => __('Three Col CTA'),
      'description'   => __('a block that is 3 columns with text and a CTA'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg viewBox="0 0 448 512"><path fill="currentColor" d="M384 32H64C28.65 32 0 60.65 0 96v320c0 35.35 28.65 64 64 64h320c35.35 0 64-28.65 64-64V96c0-35.35-28.65-64-64-64zM128 192c-17.67 0-32-14.33-32-32s14.33-32 32-32 32 14.33 32 32-14.33 32-32 32zm96 96c-17.67 0-32-14.33-32-32s14.33-32 32-32 32 14.33 32 32-14.33 32-32 32zm96 96c-17.67 0-32-14.33-32-32s14.33-32 32-32 32 14.33 32 32-14.33 32-32 32z"></path></svg>',
      'keywords'      => array( 'three', 'cta', 'text'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));


    // register a gravity form
    acf_register_block(array(
      'name'        => 'gravity-form',
      'title'       => __('Gravity Form'),
      'description'   => __('a block to embed a gravity form'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-layout',
      'mode' => 'edit',
      'icon'        => '<svg viewBox="0 0 448 512"><path fill="currentColor" d="M448 75.2v361.7c0 24.3-19 43.2-43.2 43.2H43.2C19.3 480 0 461.4 0 436.8V75.2C0 51.1 18.8 32 43.2 32h361.7c24 0 43.1 18.8 43.1 43.2zm-37.3 361.6V75.2c0-3-2.6-5.8-5.8-5.8h-9.3L285.3 144 224 94.1 162.8 144 52.5 69.3h-9.3c-3.2 0-5.8 2.8-5.8 5.8v361.7c0 3 2.6 5.8 5.8 5.8h361.7c3.2.1 5.8-2.7 5.8-5.8zM150.2 186v37H76.7v-37h73.5zm0 74.4v37.3H76.7v-37.3h73.5zm11.1-147.3l54-43.7H96.8l64.5 43.7zm210 72.9v37h-196v-37h196zm0 74.4v37.3h-196v-37.3h196zm-84.6-147.3l64.5-43.7H232.8l53.9 43.7zM371.3 335v37.3h-99.4V335h99.4z"></path></svg>',
      'keywords'      => array( 'gravity', 'form', 'embed'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));



    //--------------------------------
    // HOME PAGE ONLY LAYOUT BLOCKS
    //--------------------------------

    // register the home hero block
    acf_register_block(array(
      'name'        => 'home-hero',
      'title'       => __('Home Page Hero'),
      'description'   => __('the hero that should only be used on the home page'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-home',
      'mode' => 'edit',
      'icon'        => '<svg aria-hidden="true" viewBox="0 0 640 512"><path fill="currentColor" d="M320.67 64c-442.6 0-357.57 384-158.46 384 39.9 0 77.47-20.69 101.42-55.86l25.73-37.79c15.66-22.99 46.97-22.99 62.63 0l25.73 37.79C401.66 427.31 439.23 448 479.13 448c189.86 0 290.63-384-158.46-384zM184 308.36c-41.06 0-67.76-25.66-80.08-41.05-5.23-6.53-5.23-16.09 0-22.63 12.32-15.4 39.01-41.05 80.08-41.05s67.76 25.66 80.08 41.05c5.23 6.53 5.23 16.09 0 22.63-12.32 15.4-39.02 41.05-80.08 41.05zm272 0c-41.06 0-67.76-25.66-80.08-41.05-5.23-6.53-5.23-16.09 0-22.63 12.32-15.4 39.01-41.05 80.08-41.05s67.76 25.66 80.08 41.05c5.23 6.53 5.23 16.09 0 22.63-12.32 15.4-39.02 41.05-80.08 41.05z"></path></svg>',
      'keywords'      => array( 'home', 'hero', 'images'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));


    //--------------------------------
    // NEWS PAGE ONLY LAYOUT BLOCKS
    //--------------------------------

    // register the news stories
    acf_register_block(array(
      'name'        => 'news-stories',
      'title'       => __('News Stories'),
      'description'   => __('a block that displays a list of news stories'),
      'render_callback' => 'my_acf_block_render_callback',
      'category'      => 'block-news',
      'mode' => 'edit',
      'icon'        => '<svg viewBox="0 0 576 512"><path fill="currentColor" d="M552 64H112c-20.858 0-38.643 13.377-45.248 32H24c-13.255 0-24 10.745-24 24v272c0 30.928 25.072 56 56 56h496c13.255 0 24-10.745 24-24V88c0-13.255-10.745-24-24-24zM48 392V144h16v248c0 4.411-3.589 8-8 8s-8-3.589-8-8zm480 8H111.422c.374-2.614.578-5.283.578-8V112h416v288zM172 280h136c6.627 0 12-5.373 12-12v-96c0-6.627-5.373-12-12-12H172c-6.627 0-12 5.373-12 12v96c0 6.627 5.373 12 12 12zm28-80h80v40h-80v-40zm-40 140v-24c0-6.627 5.373-12 12-12h136c6.627 0 12 5.373 12 12v24c0 6.627-5.373 12-12 12H172c-6.627 0-12-5.373-12-12zm192 0v-24c0-6.627 5.373-12 12-12h104c6.627 0 12 5.373 12 12v24c0 6.627-5.373 12-12 12H364c-6.627 0-12-5.373-12-12zm0-144v-24c0-6.627 5.373-12 12-12h104c6.627 0 12 5.373 12 12v24c0 6.627-5.373 12-12 12H364c-6.627 0-12-5.373-12-12zm0 72v-24c0-6.627 5.373-12 12-12h104c6.627 0 12 5.373 12 12v24c0 6.627-5.373 12-12 12H364c-6.627 0-12-5.373-12-12z"></path></svg>',
      'keywords'      => array( 'news', 'stories', 'list'),
      'example' => [
        'attributes' => [
          'mode' => 'preview',
          'data' => ['is_example' => true],
        ]
      ]
    ));

  }
}