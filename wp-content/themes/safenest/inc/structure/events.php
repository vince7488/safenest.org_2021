<?php
/**
 * Event Posts functions
 *
 * 
 * @package lawfirm
 */





if ( ! function_exists( 'safenest_load_all_event_posts' ) ) :
/**
 * Load all event posts
 * 
 * @since 1.0.0
 */
function safenest_load_all_event_posts() {


    $event_args = array(
        'order' => 'DESC',
        'post_type' => 'events',
        'posts_per_page' => -1,
        'paged' => 1
    );
    $event_posts = new WP_Query($event_args); 


    if ( $event_posts->have_posts() ): ?>
      <div class="all-events wrap">
      <?php while ( $event_posts->have_posts() ): $event_posts->the_post(); 

      $post_id = get_the_id();
      $snippet = get_field('event_snippet', $post_id);
      $title = get_the_title();
      $link = get_permalink();

      //date format
      $start_date = get_field('event_start_date', $post_id);
      $end_date = get_field('event_end_date', $post_id);

      //image
      $image = get_field('event_image', $post_id);

      ?>

        <article>
          <span class="top">
          <?php if($end_date){ ?>
            <p class="date"><?= $start_date ?> - <?= $end_date ?></p>
          <?php } else { ?>
            <p class="date"><?= $start_date ?></p>
            <?php } ?>
            <div class="image" style="background:url('<?= $image['url'] ?>') center no-repeat; background-size:cover;"></div>
            <h4><?= $title ?></h4>
            <p><?= $snippet ?></p>
          </span>
          <a href="<?= $link ?>" class="link">View Event Detail</a>
        </article>

      <?php endwhile; wp_reset_postdata(); ?>
      </div>
    <?php endif;

}
endif;






if ( ! function_exists( 'safenest_load_three_event_posts' ) ) :
  /**
   * Load a featured three event callout block
   * 
   * @since 1.0.0
   */
  function safenest_load_three_event_posts() { 
    
    $featured_event_one = get_field('events_list_featured_event_one');
    $featured_event_two = get_field('events_list_featured_event_two');
    $featured_event_three = get_field('events_list_featured_event_three');
    $featured_events = Array($featured_event_one, $featured_event_two, $featured_event_three);
  ?>
  
  <div class="featured-events wrap">

  <?php 
  foreach($featured_events as $featured_event){ 
    
    $post_id = $featured_event->ID;
    $snippet = get_field('event_snippet', $post_id);
    $title = $featured_event->post_title;
    $link = get_permalink($post_id);

    //date format
    $start_date = get_field('event_start_date', $post_id);
    $end_date = get_field('event_end_date', $post_id);

    //image
    $image = get_field('event_image', $post_id);
  ?>

    <article>
      <span class="top">
      <?php if($end_date){ ?>
        <p class="date"><?= $start_date ?> - <?= $end_date ?></p>
      <?php } else { ?>
        <p class="date"><?= $start_date ?></p>
        <?php } ?>
        <div class="image" style="background:url('<?= $image['url'] ?>') center no-repeat; background-size:cover;"></div>
        <h4><?= $title ?></h4>
        <p><?= $snippet ?></p>
      </span>
      <a href="<?= $link ?>" class="link">View Event Detail</a>
    </article>

  <?php } ?>
  </div>

  <a href="/how-to-help/events/" class="btn-purple">View All Events</a>

  <?php 
  }
  endif;



