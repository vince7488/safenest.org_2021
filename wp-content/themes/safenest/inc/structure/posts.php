<?php
/**
 * News and Posts functions
 *
 * 
 * @package lawfirm
 */





if ( ! function_exists( 'lawfirm_get_total_post_count' ) ) :
  /**
   * Get the total amount of news posts
   * @since 1.0.0
   */
  function lawfirm_get_total_post_count() {
    $args = array(
      'order' => 'DESC',
      'posts_per_page' => -1
    );
  $news_posts = new WP_Query($args); 
  $num_posts = $news_posts->post_count;
  //wp_reset_postdata();
  return $num_posts;
  }
endif;



if ( ! function_exists( 'lawfirm_get_total_post_count_archive' ) ) :
  /**
   * Get the total amount of news posts on an archive page
   * @since 1.0.0
   */
  function lawfirm_get_total_post_count_archive() {
    
    while ( have_posts() ) : the_post();
    global $wp_query;
    $total_posts = $wp_query->found_posts;
    endwhile;

    return $total_posts;

  }
endif;


if ( ! function_exists( 'safenest_get_total_news_page_count' ) ) :
  /**
   * Get the total amount of news posts
   * @since 1.0.0
   */
  function safenest_get_total_news_page_count() {
    $args = array(
      'order' => 'DESC',
      'posts_per_page' => -1
    );
  $news_posts = new WP_Query($args); 
  $total_pages = $news_posts->max_num_pages;
  //wp_reset_postdata();
  return $total_pages;
  }
endif;




if ( ! function_exists( 'safenest_load_news_posts' ) ) :
/**
 * Load all news posts via ajax
 * 
 * @since 1.0.0
 */
function safenest_load_news_posts() {

    $paged = ( isset( $_GET['page'] ) ) ? $_GET['page'] : 1;

    $news_args = array(
        'order' => 'DESC',
        'posts_per_page' => 6,
        'paged' => $paged
    );
    $news_posts = new WP_Query($news_args); 

    if ( $news_posts->have_posts() ): ?>
      <?php while ( $news_posts->have_posts() ): $news_posts->the_post(); 

        //post info
        $post_id = get_the_ID();
        $snippet = get_field('news_post_snippet', $post_id);
        $title = get_the_title();
        $date = get_the_date();
        $type = get_field('news_post_post_type', $post_id);

        //get story image info
        $card_image = get_field('news_post_card_image', $post_id);
        $url = $card_image['url'];
        $alt = $card_image['alt'];
        $size = 'lawfirm_img_large';
        $thumb = $card_image['sizes'][ $size ];

        //set link and button text
        $link = get_permalink();
        $button_text = "Read More";
        if($type == "external_video"){
          $link = get_field('news_post_external_url', $post_id);
          $button_text = "Watch Video";
        }
        if($type == "external_story"){
          $link = get_field('news_post_external_url', $post_id);
          $button_text = "View Story";
        }
    ?>

        <article>
          <span>
            <img src="<?= esc_url($thumb); ?>" alt="<?= esc_attr($alt); ?>" />
          </span>
          <span>
            <p class="info"><?= $date ?> | <a href="#">Category</a></p>
              <a href="<?= $link ?>"><h2><?= $title ?></h2></a>
            <p class='snippet'><?= $snippet ?></p>

            <?php 
            //load the button variant
            if ($type == "external_video" || $type == "external_story"){ ?>
              <a href="<?= $link ?>" class="btn-purple-sm"><?= $button_text ?><?= file_get_contents(get_template_directory_uri() . "/src/icons/exit-link.svg", false, stream_context_create($arrContextOptions)); ?></a>
            <?php } else { ?>
              <a href="<?= $link ?>" class="btn-purple-sm"><?= $button_text ?></a>
            <?php } ?>
          </span>
        </article>


      <?php endwhile; wp_reset_postdata(); ?>
    <?php endif;
    //die();
}
endif;





if ( ! function_exists( 'lawfirm_load_archive_posts' ) ) :
/**
 * Load posts on an archive page
 * @since 1.0.0
 */
function lawfirm_load_archive_posts() {
  $paged = $_POST['paged'];
  
  if ( have_posts() ) :
    while ( have_posts() ) : the_post();
    $post_id = get_the_id();
    $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'lawfirm_img_large' , false, '' );
      //get the location
      $location = get_field('location', $post_id);
      $location_post = $location;
      setup_postdata( $location_post );
      $city = get_the_title($location_post);
      $state = get_the_terms( $location_post, 'location-state' );
      $location_link = get_permalink($location_post->ID);

      //display all the stuffs
      ?>
      <article class="entry news-post-entry archive-post-entry">
        
        <div class="blog-featured-img" style="background:#eeeeee url('<?php echo $featured_image[0]; ?>') center no-repeat; background-size:cover;">
          <img src="<?php echo $featured_image[0]; ?>" alt="<?php the_title(); ?>" class="entry-image" width=387 height=199 style="opacity:0;">
        </div>

        <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        
        <p class="entry-meta date-location"><?php echo get_the_date('m.d.y'); ?>
          <?php 
          if ( !in_category('nationwide') ){
            printf('&nbsp;&nbsp;|&nbsp;&nbsp;<a href="%s"> %s, %s</a>',$location_link, $city, $state[0]->name); 
          } else {
            echo('&nbsp;&nbsp;|&nbsp;&nbsp;Nationwide');
          }
          ?>
        </p>

        <?php 
          $excerpt = get_the_excerpt();
          $desc = lawfirm_concat_search_description($excerpt);
          ?>
          <p><?php echo $desc; ?>
          <a href="<?php the_permalink(); ?>">Read More ></a>
          </p>
      </article>
     <?php
    endwhile;
  endif;
  wp_reset_postdata();
}
endif;



