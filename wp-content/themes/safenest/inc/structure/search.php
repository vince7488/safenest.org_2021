<?php
/**
 * Search 
 *
 * 
 * @package lawfirm
 */


if ( ! function_exists( 'safenest_load_search_posts' ) ) :
/**
 * Load posts a search page
 * @since 1.0.0
 */
function safenest_load_search_posts() {

  if ( have_posts() ) {
    while ( have_posts() ) : the_post();
    
    $post_id = get_the_ID();
    $type = get_post_type();
    //
    //
    // PAGE
    //
    //
    if($type == "page"){ 

      $title = get_the_title();
      $permalink = get_page_link($post_id);

      //set generic values for this search card
      $hero_title = $title;
      $hero_copy = "There is no description provided for this page";
      //loop through the blocks that exist in the page content and find some meaningful information that we can display on the search card
      if ( function_exists( 'get_field' ) ) {
        $pid = get_post();
        if ( has_blocks( $pid_content ) ) {
          $blocks = parse_blocks( $pid->post_content );
          foreach ( $blocks as $block ) {
            //if home-hero exists
            if ($block['blockName'] == 'acf/home-hero'){
              $hero_title = $block["attrs"]["data"]["home_hero_headline"];
              $hero_copy = $block["attrs"]["data"]["home_hero_copy"];
            // if internal hero exists
            } elseif ($block['blockName'] == 'acf/internal-hero'){
              $hero_title = $block["attrs"]["data"]["internal_page_hero_headline"];
              $hero_copy = $block["attrs"]["data"]["internal_page_hero_copy"];
            }
          }
        }
      }
      
      
    ?>
      <article class="entry">
        
        <span>
          <h5>Page</h5>
          <p class="date"><?= $title ?></p>
        </span>

        <span>
          <h3><?= $hero_title ?></h3>
          <p class="snippet"><?= $hero_copy ?></p>
          <a href="<?= $permalink ?>" class="link">View Page</a>
        </span>
        
      </article>
      <?php
    //
    //
    // NEWS POST
    //
    //
    } elseif ($type == "post"){ 
      
      $title = get_the_title();
      $permalink = get_permalink($post_id);
      $snippet = get_field('news_post_snippet');
      $date = get_the_date('m.d.Y');
      ?>

      <article class="entry">
        
        <span>
          <h5>News</h5>
          <p class="date"><?= $date ?></p>
        </span>

        <span>
          <h3><?= $title ?></h3>
          <p class="snippet"><?= $snippet ?></p>
          <a href="<?= $permalink ?>" class="link">Read More</a>
        </span>
        
      </article>
      <?php
    //
    //
    // EVENT POST
    //
    //
    } elseif ($type == "events"){ 

      $title = get_the_title();
      $permalink = get_permalink($post_id);
      $snippet = get_field('event_snippet');
      $start_date = get_field('event_start_date');
      $end_date = get_field('event_end_date');
      ?>

      <article class="entry">
        
        <span>
          <h5>Event</h5>
          <p class="date"><?= $start_date ?></p>
        </span>

        <span>
          <h3><?= $title ?></h3>
          <p class="snippet"><?= $snippet ?></p>
          <a href="<?= $permalink ?>" class="link">View Event Details</a>
        </span>
        
      </article>
      
      <?php } else { 
        
        $title = get_the_title();
        $permalink = get_permalink($post_id);
        ?>

        <article class="entry">
                
        <span>
          <h5><?= $title ?></h5>
        </span>

        <span>
          <h3><?= $title ?></h3>
          <p class="snippet">There is no description provided for this page</p>
          <a href="<?= $permalink ?>" class="link">View Event Details</a>
        </span>

        </article>

        <?php }

    endwhile;
  } else { 
    // results if nothing is found
    ?>
     <article>
        <p>No results were found.</p>
      </article>
  <?php
  }
  wp_reset_postdata();
}
endif;




if ( ! function_exists( 'lawfirm_concat_search_description' ) ) :
/**
 * Load posts a search page
 * @since 1.0.0
 */
function lawfirm_concat_search_description($str) {
    $limit=250;
    $strip = false;
    $str = ($strip == true)?strip_tags($str):$str;
    if (strlen ($str) > $limit) {
        $str = substr ($str, 0, $limit - 3);
        return (substr ($str, 0, strrpos ($str, ' ')).'...');
    }
    return trim($str);
}
endif;