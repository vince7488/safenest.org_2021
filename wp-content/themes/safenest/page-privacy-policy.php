<?php
/**
* The Privacy Policy template
*
* @package lawfirm
*/
get_header(); ?>

<?php
while ( have_posts() ) : the_post(); ?>
  <section class="blank privacy">
    <div class="title wrap">
      <h1><?= the_title() ?></h1>
    </div>
    <div class="wrap">
    <?php echo the_content(); ?>
    </div>
  </section>
<?php endwhile;
wp_reset_postdata();

get_footer(); ?>


