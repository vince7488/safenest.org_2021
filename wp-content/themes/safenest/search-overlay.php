<?php
/**
* The template for displaying search bar in the header.
*
*
* @package thelawfirm
*/


?>

<section class="search-pop">

  <div class="top">
    <span>
      <form action="/" method="get">
      <input class="search-bar" placeholder="Search" value="<?php the_search_query(); ?>" name="s" />
      </form>
      <p>Hit enter to search</p>
    </span>
  </div>

  <div class="bottom">
    <span>
      <h6>Suggested Searches</h6>
      <a href="/?s=get+help">Get Help</a>
      <a href="/?s=donate+now">Donate Now</a> 
      <a href="/?s=events">Events</a> 
      <a href="/?s=how+to+help">How To Help</a> 
      <a href="/?s=crisis+services">Crisis Services</a> 
      <a href="/?s=What+is+Domestic+Violence">What is Domestic Violence?</a>
  </span>
  </div>

  <svg viewbox="0 0 24 24">
    <path fill="#ffffff" d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path>
  </svg>
  
</section>
