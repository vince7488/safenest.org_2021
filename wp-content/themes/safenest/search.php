<?php
/**
* The template for displaying search results pages.
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
*
* @package thelawfirm
*/

get_header(); 


$search_results = get_search_query();
?>

<main>

<section class="search-result-page">

  <div class="search-bar">
    <div class="wrap">
      <form action="/" method="get">
        <input class="search-bar" placeholder="Search" value="" name="s" />
      </form>
      <p>Hit enter to search</p>
    </div>
  </div>

  <div class="search-header">
    <div class="wrap">
      <h2>Search Results for<br />
        <strong><?php echo "'".$search_results."'";?></strong>
      </h2>
    </div>
  </div>


  <div class="search-results wrap">
    <?php safenest_load_search_posts(); ?>
  </div>

</section>

</main>
<?php get_footer(); ?>
