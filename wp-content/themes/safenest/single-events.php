<?php
/**
* The template for displaying all single posts.
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
*
* @package thelawfirm
*/

get_header(); 

while ( have_posts() ) : the_post();

$post_id = get_the_id();
$event_title = get_the_title();
$snippet = get_field('event_snippet', $post_id);
$start_date = get_field('event_start_date', $post_id);
$end_date = get_field('event_end_date', $post_id);

$image = get_field('event_image', $post_id);
$url = $image['url'];
$alt = $image['alt'];
$size = 'lawfirm_img_large';
$thumb = $image['sizes'][ $size ];
?>

<main>
  <section class="news-single wrap">

    <div class="news-header">
      <span>
        <img src="<?= esc_url($thumb); ?>" alt="<?= esc_attr($alt); ?>" />
      </span>
      <span>
        <?php if($end_date){ ?>
        <p class="info"><?= $start_date ?> - <?= $end_date ?></p>
        <?php } else { ?>
        <p class="info"><?= $start_date ?></p>
        <?php } ?>
        <h2><?= $event_title ?></h2>
      </span>
    </div>

    <div class="news-story">

      <?php 
      if ($type == "iframe"){
        
      $iframe_code = get_field('news_post_iframe'); ?>
        <div class="iframe">
          <?= $iframe_code ?>
        </div>
      <?php } else { 
        
      $text_builder = get_field('news_post_post_content');
      ?>

        <div class="text-builder">
          <?php 
          foreach ($text_builder["safenest_text_builder"] as $item) {
  
            // Case: headline
            if( $item["acf_fc_layout"] == 'headline' ):
              $headline = $item["headline"] ?>
              <h2><?= $headline ?></h2>

            <?php
            // Case: subheadline
            elseif( $item["acf_fc_layout"] == 'subheadline' ): 
                $subheadline = $item["sub_headline"] ?>
                <h5><?= $subheadline ?></h5>

            <?php
            // Case: text block
            elseif( $item["acf_fc_layout"] == 'text_block' ): 
                $text_block = $item["text_block"] ?>
                <?= $text_block ?>

            <?php
            // Case: list
            elseif( $item["acf_fc_layout"] == 'list' ): 
                $list = $item["list"] ?>
              <ul>
                <?php
                foreach ($list as $list_item) { ?>

                    <li><?= $list_item["item"] ?></li>

                <?php } ?>
              </ul>

            <?php
            // Case: button
            elseif( $item["acf_fc_layout"] == 'button' ):  

                $link = $item["button"];
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
                <a class="btn-green"  href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>"><?= esc_html( $link_title ); ?></a>

            <?php
            endif;
          } ?>
        </div>
      <?php } ?>
    </div>

  </section>
</main>

<?php 
endwhile; // End of the loop.
wp_reset_postdata();
get_footer(); 

?>


