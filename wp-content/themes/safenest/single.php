<?php
/**
* The News Single Page
*
* @package lawfirm
*/
get_header(); 

$post_id = get_the_ID();
$type = get_field('news_post_post_type', $post_id);
$title = get_the_title();
$date = get_the_date();

//get story image info
$card_image = get_field('news_post_card_image', $post_id);
$url = $card_image['url'];
$alt = $card_image['alt'];
$size = 'lawfirm_img_large';
$thumb = $card_image['sizes'][ $size ];
?>

<main>
  <section class="news-single wrap">

    <div class="news-header">
      <span>
        <img src="<?= esc_url($thumb); ?>" alt="<?= esc_attr($alt); ?>" />
      </span>
      <span>
        <p class="info"><?= $date ?></p>
          <h2><?= $title ?></h2>
        <p class='snippet'>Category</p>
      </span>
    </div>

    <div class="news-story">

      <?php 
      if ($type == "iframe"){
        
      $iframe_code = get_field('news_post_iframe'); ?>
        <div class="iframe">
          <?= $iframe_code ?>
        </div>
      <?php } else { 
        
      $text_builder = get_field('news_post_post_content');
      //var_dump($text_builder);
      ?>

        <div class="text-builder">
          <?php 
          foreach ($text_builder["safenest_text_builder"] as $item) {
  
            // Case: headline
            if( $item["acf_fc_layout"] == 'headline' ):
              $headline = $item["headline"] ?>
              <h2><?= $headline ?></h2>

            <?php
            // Case: subheadline
            elseif( $item["acf_fc_layout"] == 'subheadline' ): 
                $subheadline = $item["sub_headline"] ?>
                <h5><?= $subheadline ?></h5>

            <?php
            // Case: text block
            elseif( $item["acf_fc_layout"] == 'text_block' ): 
                $text_block = $item["text_block"] ?>
                <?= $text_block ?>

            <?php
            // Case: list
            elseif( $item["acf_fc_layout"] == 'list' ): 
                $list = $item["list"] ?>
              <ul>
                <?php
                foreach ($list as $list_item) { ?>

                    <li><?= $list_item["item"] ?></li>

                <?php } ?>
              </ul>

            <?php
            // Case: button
            elseif( $item["acf_fc_layout"] == 'button' ):  

                $link = $item["button"];
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
                <a class="btn-green"  href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>"><?= esc_html( $link_title ); ?></a>

            <?php
            endif;
          } ?>
        </div>
      <?php } ?>
    </div>

  </section>
</main>

<?php get_footer(); ?>


