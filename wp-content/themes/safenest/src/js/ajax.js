/*

  SafeNest

  Site Development by R&R Partners
  Developer: Daniel Freeman [@dFree]

*/

document.addEventListener("DOMContentLoaded", function(){

//running that sweet jQuery for the simple ajax functionality
(function ($) {

  //don't need this url if you are using ajaxflow
  var ajax_url = $('.global-footer').data('ajaxurl');
  
  $('.load-more').on('click', function(){
  
    var button = $(this);
    var page = button.data('page');
    console.log("Page:" + page);
    var totalPages = $('.post-container').data("total");
    console.log("Total Pages:" + totalPages);
    page++;
   
  
  
    $.ajax({
        url : '/ajaxflow/safenest_load_news_posts',
        type : 'GET',
        data: {
          page: page
        },
        beforeSend : function ( xhr ) {
          button.text('Loading...');
          button.data( "page", page );
        },
        success: function( data ) {
          if(page < totalPages){
            button.text('Load More');
            $('.post-container').append(data);
          } else {
            button.remove();
            $('.post-container').append(data);
            var end = "<h5 class='no-more-posts'>You've reached the end of our news articles.</h5>"
            $('.post-container').append(end);
          }
        }
      });
  });
  
})(jQuery); //end jquery
});//end