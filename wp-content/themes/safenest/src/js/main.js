/*

  SafeNest

  Site Development by R&R Partners
  Developer: Daniel Freeman [@dFree]

*/

document.addEventListener('DOMContentLoaded', function () {
  // WINDOW RESIZE FUNCTIONS -----------------------
  var windowWidth = window.outerWidth,
    windowHeight = window.outerHeight

  window.addEventListener('resize', function () {
    windowWidth = window.outerWidth
    windowHeight = window.outerHeight
  })

  //returns the direct path to the theme
  function getHomeUrl() {
    var href = window.location.hostname
    if (href == 'boiler.test') {
      var index = 'http://' + href + '/wp-content/themes/boiler'
    } else {
      var index = 'https://' + href + '/wp-content/themes/boiler'
    }
    return index
  }

  // HEADER NAVIGATION HOVER MENUS  -----------------------

  // HAM ICON  -----------------------
  var hamIcon = document.querySelector('.ham')
  hamIcon.addEventListener('click', toggleMenu)
  function toggleMenu() {
    //switch ham icon
    hamIcon.classList.toggle('ham-close')
    //open menu
    var navMenu = document.querySelector('.nav-mobile')
    navMenu.classList.toggle('nav-mobile-open')
    //make logo white
    var whiteLogo = document.querySelector('.white')
    var colorLogo = document.querySelector('.color')
    whiteLogo.classList.toggle('white-open')
    colorLogo.classList.toggle('color-open')
  }

  // MOBILE NAV ACCORDIONS  -----------------------
  var mobileAccords = document.querySelectorAll('.nav-accord')
  mobileAccords.forEach(function (item) {
    var mainSpan = item.querySelector('.main span')
    if (mainSpan) {
      mainSpan.addEventListener('click', openMobileAcc)
    }
  })
  function openMobileAcc() {
    this.parentNode.parentNode.classList.toggle('active')
  }

  // ACCORDIONS  -----------------------
  var hasAccordions = document.querySelector('.accordion-link-list')
  if (hasAccordions) {
    var accItems = document.querySelectorAll('.accordion h6')
    accItems.forEach(function (acc) {
      acc.addEventListener('click', openAcc)
    })

    function openAcc() {
      this.parentNode.classList.toggle('accordion-open')
    }
  }

  // SEARCH BUTTON  -----------------------
  NodeList.prototype.forEach = Array.prototype.forEach
  var searchIcons = document.querySelectorAll('.search-icon')

  searchIcons.forEach(function (item) {
    //event listerner for the main nav search icon
    item.addEventListener('click', addSearch)
  })

  //event listener for the mobile search icon
  document.getElementById('search-icon-mobile').addEventListener('click', addSearch)

  //event listener for the close button on the search popout
  document.querySelector('.search-pop svg').addEventListener('click', removeSearch)

  function addSearch() {
    var searchPop = document.querySelector('.search-pop')
    var mainShrink = document.querySelector('main')

    searchPop.classList.remove('search-pop-close')
    mainShrink.classList.remove('search-shrink-open')

    searchPop.classList.toggle('search-pop-open')
    mainShrink.classList.toggle('search-shrink-close')
  }
  function removeSearch() {
    var searchPop = document.querySelector('.search-pop')
    var mainShrink = document.querySelector('main')

    searchPop.classList.toggle('search-pop-open')
    mainShrink.classList.toggle('search-shrink-open')

    searchPop.classList.toggle('search-pop-close')
    mainShrink.classList.toggle('search-shrink-close')
  }

  // TABS  -----------------------
  var hasTabs = document.querySelector('.tabbed-image-with-text')
  if (hasTabs) {
    //add click listener to all tab buttons
    var tabs = document.querySelectorAll('.tabbed-image-with-text .tabs>ul>li')
    tabs.forEach(function (tab) {
      tab.addEventListener('click', switchTab)
    })

    function switchTab() {
      var clickedTab = this
      var tabNum = clickedTab.id.toString()

      //remove active from all tabs
      tabs.forEach(function (tab) {
        tab.classList.remove('active')
      })
      //add active on the button that was clicked
      clickedTab.classList.add('active')

      //remove all active content blocks
      var tabContentBlocks = document.querySelectorAll('.tabbed-image-with-text span')
      tabContentBlocks.forEach(function (el) {
        el.classList.remove('active-tab')
      })
      // add new active content block
      var tabName = '.' + tabNum
      var newActiveBlock = document.querySelectorAll(tabName)
      newActiveBlock[0].classList.add('active-tab')
    }
  }

  // ANIMATIONS  -----------------------
  // init scrollmagic controller
  var controller = new ScrollMagic.Controller()

  // lets only run this if a home-hero element is on the page
  // otherwise we are going to have a different navigation that doesn't scroll
  var pageIsHome = document.body.classList.contains('page-home')
  if (pageIsHome) {
    //header scroll animation

    //at 1060 breakoint we need to change the final height of the element
    var shrinkHeight = '0px'

    if (windowWidth > 321 && windowWidth < 1023) {
      shrinkHeight = '90px'
    } else if (windowWidth > 1024 && windowWidth < 1366) {
      shrinkHeight = '95px'
    } else {
      shrinkHeight = '110px'
    }

    var navSwtichTween = gsap.to('.global-header', {
      duration: 0.2,
      ease: Power1.easeInOut,
      backgroundColor: '#ffffff',
      height: shrinkHeight,
    })
    var nav_ani = new ScrollMagic.Scene({
      duration: 600,
      triggerElement: '#nav-trigger',
      triggerHook: -200,
      // reverse: false
    })
      .setTween(navSwtichTween)
      //.addIndicators() // add indicators
      .addTo(controller)

    //nav elements
    var navTween = gsap.to('.nav-desk nav .nav-box .top-link a', {
      duration: 0.1,
      ease: Power1.easeInOut,
      color: '#000000',
      translateY: '-7px',
    })
    var nav_ani = new ScrollMagic.Scene({
      duration: 500,
      triggerElement: '#nav-trigger',
      triggerHook: -200,
    })
      .setTween(navTween)
      //.addIndicators() // add indicators
      .addTo(controller)

    //donate now button
    var donateNavTween = gsap.to('.nav-desk nav .btn-green-sm', {
      duration: 0.1,
      ease: Power1.easeInOut,
      translateY: '-7px',
    })
    var donante_nav_ani = new ScrollMagic.Scene({
      duration: 500,
      triggerElement: '#nav-trigger',
      triggerHook: -200,
    })
      .setTween(donateNavTween)
      //.addIndicators() // add indicators
      .addTo(controller)

    var navSVGTween = gsap.to('.nav-desk nav svg', {
      duration: 0.1,
      ease: Power1.easeInOut,
      marginTop: '-5px',
    })
    var nav_ani = new ScrollMagic.Scene({
      duration: 500,
      triggerElement: '#nav-trigger',
      triggerHook: -200,
    })
      .setTween(navSVGTween)
      //.addIndicators() // add indicators
      .addTo(controller)

    //nav white logo switch
    var navLogo = gsap.to('.global-header .logo .white', {
      duration: 0.2,
      ease: Power1.easeInOut,
      opacity: 0,
    })
    var nav_ani = new ScrollMagic.Scene({
      duration: 600,
      triggerElement: '#nav-trigger',
    })
      .setTween(navLogo)
      .addTo(controller)

    //nav logo size change
    //at 1060 breakoint we need to change the logo placment
    var shrinkWidth = '160px'
    var shrinkMargin = '14px'
    if (windowWidth < 1230) {
      shrinkWidth = '250px'
      shrinkMargin = '17px;'
    }
    if (windowWidth < 1060) {
      shrinkWidth = '142px'
      shrinkMargin = '12px;'
    }

    var navLogoShrink = gsap.to('.global-header .logo', {
      duration: 0.2,
      ease: Power1.easeInOut,
      width: shrinkWidth,
      marginTop: shrinkMargin,
    })
    var nav_ani = new ScrollMagic.Scene({
      duration: 600,
      triggerElement: '#nav-trigger',
    })
      .setTween(navLogoShrink)
      .addTo(controller)

    //nav hotline change
    var navActionToggle = gsap.to('.global-header .action', {
      duration: 0.2,
      ease: Power1.easeInOut,
      opacity: '0',
      translateY: '0px',
    })
    var nav_ani = new ScrollMagic.Scene({
      duration: 600,
      triggerElement: '#nav-trigger',
    })
      .setTween(navActionToggle)
      .addTo(controller)

    //add green callout
    var navCalloutToggle = gsap.to('.global-header .green-callout', {
      duration: 0.2,
      ease: Power1.easeInOut,
      top: '0px',
    })
    var nav_ani = new ScrollMagic.Scene({
      duration: 600,
      triggerElement: '#nav-trigger',
    })
      .setTween(navCalloutToggle)
      .addTo(controller)

    //search button fill color
    var navSearch = gsap.to('.global-header nav svg path', {
      duration: 0.2,
      ease: Power1.easeInOut,
      fill: '#000000',
    })
    var nav_ani = new ScrollMagic.Scene({
      duration: 600,
      triggerElement: '#nav-trigger',
    })
      .setTween(navSearch)
      .addTo(controller)
  }

  // jQuery for Slick or anything else that might require thee ol jQuery
  ;(function ($) {
    // SLICK SLIDER  -----------------------
    var gallery = document.getElementById('gallery')
    if (gallery) {
      //slick init
      $('#gallery').slick({
        arrows: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        centerMode: true,
        variableWidth: true,
      })

      //video element play
      // TODO : make this work for multiple video, add pause button control, add on end function, if gallery is slide make poster and text come back up
      $('.video img').on('click', function () {
        $('.video .text, .video .cover, .video img').fadeOut()
        var video = document.getElementById('video-1')
        video.play()
      })
    }

    //testimonial slider
    var testimonials = document.getElementById('testimonials')
    if (testimonials) {
      $('#testimonials').slick({
        arrows: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        adaptiveHeight: true,
      })
    }

    //history slider
    var historySlider = document.getElementById('history')
    if (historySlider) {
      $('#history').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),
        responsive: [
          {
            breakpoint: 1000,
            settings: {
              infinite: true,
              slidesToShow: 2,
              slidesToScroll: 1,
              dots: false,
              arrows: false,
            },
          },
          {
            breakpoint: 500,
            settings: {
              infinite: true,
              slidesToShow: 1,
              slidesToScroll: 1,
              dots: false,
              arrows: false,
            },
          },
        ],
      })
    }
  })(jQuery) //end jquery

  // badhawk tag!!
  console.log('--------------------------------------')
  console.log('-=      built by rrpartners.com     =-')
  console.log('--------------------------------------')
}) //end
