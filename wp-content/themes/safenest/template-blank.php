<?php
/**
* The Privacy Policy template
*
* @package lawfirm
*/
get_header(); ?>

<section class="tac-header">
  <span class="wrap">
    <h1><?= the_title() ?></h1>
  </span>
</section>

<?php
while ( have_posts() ) : the_post(); ?>
  <div class="wrap">
    <?php echo the_content(); ?>
  </div>
<?php endwhile;
wp_reset_postdata();

get_footer(); ?>


